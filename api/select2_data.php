<?php
include("conn.php");

header("Content-Type: application/json");

$select2_sql = "SELECT mslink as id, nama as text, tanggal as updated_date FROM balai ORDER BY tanggal DESC";
$select2_sql_result = $conn->query($select2_sql);

$selectors = array();
while($r = mysqli_fetch_assoc($select2_sql_result)) {
  $selectors[] = $r;
}

if (!isset($_GET['balai_id']) || $_GET['balai_id'] == '0') {
  $selectors[0]['selected'] = true;
  $latest_date = $selectors[0]['updated_date'];
} else {
  foreach ($selectors as $index => $balai) {
    if ($balai['id'] == $_GET['balai_id']) {
      $selectors[$index]['selected'] = true;
      $latest_date = $selectors[$index]['updated_date'];
    }
  }
}

$response = array(
  'selectors' => $selectors,
  'latest_date' => $latest_date,
);

echo json_encode($response);

$conn->close();