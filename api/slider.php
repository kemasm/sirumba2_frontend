<?php
include("conn.php");

header("Content-Type: application/json");

if (isset($_GET['marker_id'])) {
    $marker_id = $_GET['marker_id'];
} else {
    $marker_id = "0";
}

$sql = "SELECT tag, thumbnail, src FROM bencana_marker_item WHERE bencana_marker_id={$marker_id} ";
$result = $conn->query($sql);

$response = array();
while($r = mysqli_fetch_assoc($result)) {
    $response[] = $r;
}

echo json_encode($response);

$conn->close();
