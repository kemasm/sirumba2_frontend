<?php
include("conn.php");

header("Content-Type: application/json");

if (isset($_GET['bencana_id'])) {
  $bencana_id = $_GET['bencana_id'];
} else {
  $bencana_id = "1";
}

$bencana_marker_sql = "SELECT id, lat, lng FROM bencana_marker WHERE bencana_id={$bencana_id}";
$bencana_marker_sql_result = $conn->query($bencana_marker_sql);

$map_center_sql = "SELECT y, x FROM bencana WHERE mslink={$bencana_id}";
$map_center_sql_result = $conn->query($map_center_sql);

$center = array();
$markers = array();

while($r = mysqli_fetch_assoc($bencana_marker_sql_result)) {
  $markers[] = $r;
}
while($r = mysqli_fetch_assoc($map_center_sql_result)) {
  $center[] = $r;
}

foreach ($markers as $key => $marker){
  $markers[$key]['latLng'] = array($marker['lat'], $marker['lng']);
  unset($markers[$key]['lat']);
  unset($markers[$key]['lng']);
}

$response = array(
  'center' => array($center[0]['y'], $center[0]['x']),
  'markers' => $markers
);

echo json_encode($response);

$conn->close();