$(function () {
    var backgroundColor = ['#5D6E76', '#F8C114', '#F47D20', '#6CADBF', 'lightgrey']

    var pieChartOptions = {
        layout: {
            padding: {
                left: 20,
                bottom: 20
            }
        },
        maintainAspectRatio: false,
        responsive: true,
        legend: {
            position: 'right',
            labels: {
                usePointStyle: true
            },
        },
        plugins: {
            datalabels: {
                anchor: 'end',
                align: 'end',
            }
        }
    }

    var barChartOptions = {
        layout: {
            padding: {
                top: 25,
            }
        },
        responsive: true,
        maintainAspectRatio: false,
        datasetFill: false,
        legend: {
            display: false
        },
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true,
                }
            }]
        },
        plugins: {
            datalabels: {
                anchor: 'end',
                align: 'end',
            }
        }
    }

    class ChartBuilder {
        constructor(type, selector, chart_data) {
            this.type = type
            this.selector = selector
            this.chart_data = chart_data
            if (this.type === 'pie') {
                this.chartOptions = pieChartOptions
                this.data = {
                    datasets: [{backgroundColor: backgroundColor}]
                }
            } else {
                this.chartOptions = barChartOptions
                this.data = {
                    datasets: [{backgroundColor: backgroundColor, maxBarThickness: 40,}]
                }
            }
        }

        fill_chart_data(chart_data, target_chart) {
            target_chart.data.labels = chart_data.labels;
            target_chart.data.datasets[0].data = chart_data.data;
            target_chart.update();
        }

        build() {
            var chartCanvas = $(this.selector).get(0).getContext('2d')
            var created_chart = new Chart(chartCanvas, {
                type: this.type,
                data: this.data,
                options: this.chartOptions
            })
            this.fill_chart_data(this.chart_data, created_chart);
            return created_chart;
        }
    }

    // first page chart

    var pieChart = new ChartBuilder('pie', '#pieChart', pieChartData).build();
    var barChart = new ChartBuilder('bar', '#barChart', barChartData).build();

    // second page chart
    var barChartRumah = new ChartBuilder('bar', '#barChart-rumah', barChartRumahData).build();
    var barChartStatusRumah = new ChartBuilder('bar', '#barChart-status-rumah', barChartStatusRumahData).build();
    var barChartStatusLahan = new ChartBuilder('bar', '#barChart-status-lahan', barChartStatusLahanData).build();
    var pieChartGender = new ChartBuilder('pie', '#pieChart-gender', pieChartGenderData).build();
})