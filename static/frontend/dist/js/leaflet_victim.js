$(function () {
    var victim_data = {
        'center': [39.73, -104.99],
        'markers': [
            {'nik': '123456789', 'latLng': [39.61, -105.02]},
            {'nik': '123456788', 'latLng': [39.74, -104.99]},
            {'nik': '123456787', 'latLng': [39.73, -104.8]},
        ]
    };

    $.ajax({
        type: 'GET',
        url: 'api/peta_korban.php',
        async: false,
        data: {
            bencana_id: bencana_id,
        },
        success: function (response) {
            victim_data = response;
        }
    })

    var openstreetmap2 = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    });

    var secondMap = L.map('second-map', {
        center: victim_data.center,
        zoom: 10,
        layers: [openstreetmap2],
        zoomControl: false
    });

    L.easyButton({
        position: 'bottomright',
        states: [{                 // specify different icons and responses for your button
            stateName: 'get-center',
            onClick: function (btn, map) {
                map.setView(victim_data.center, 10);
            },
            title: 'Zoom To Home',
            icon: 'fa-crosshairs'
        }]
    }).addTo(secondMap);
    L.control.zoom({position: 'bottomright'}).addTo(secondMap);

    var cities2 = L.layerGroup();

    $.each(victim_data.markers, function (index, value) {
        var marker = L.marker(value.latLng);
        marker.addTo(secondMap);
        marker.addTo(cities2);

        marker.on('click', function () {
            $('#modal-xl').modal('show');
        });
    })

    var baseLayers2 = {
        "Openstreetmap": openstreetmap2,
    };

    var overlays2 = {
        "Korban": cities2.addTo(secondMap)
    };

    L.control.layers(baseLayers2, overlays2).addTo(secondMap);

    $('a[data-toggle="pill"]').on('shown.bs.tab', function () {
        secondMap.invalidateSize(false);
    })

    var autoCompleteJS = new autoComplete({
        data: {
            src: async () => {
                const query = document.querySelector("#autoComplete").value;
                const source = await fetch(`/api/cari_korban.php?query=${query}`);
                const data = await source.json();
                return data.results;
            },
            key: ['nik', 'nama'],
            cache: false
        },
        placeHolder: "masukkan nik atau nama",
        selector: "#autoComplete",
        threshold: 3,
        maxResults: 5,
        resultItem: {
            content: (data, element) => {
                element.style = "display: flex; justify-content: space-between; pointer-events: initial;";
                element.innerHTML = `<span style="text-overflow: ellipsis; white-space: nowrap; overflow: hidden;">${data.value.nik} - ${data.value.nama}</span>`;
            },
        },
        noResults: (dataFeedback, generateList) => {
            generateList(autoCompleteJS, dataFeedback, dataFeedback.results);
            const result = document.createElement("li");
            result.setAttribute("class", "no_result");
            result.setAttribute("tabindex", "1");
            result.innerHTML = `<span style="display: flex; align-items: center; font-weight: 100; color: rgba(0,0,0,.2);">Found No Results for "${dataFeedback.query}"</span>`;
            document.querySelector(`#${autoCompleteJS.resultsList.idName}`).appendChild(result);
        },
        onSelection: feedback => {
            secondMap.panTo(feedback.selection.value.latLng);
            $('#modal-xl').modal('show');
        }
    })

});