$(function () {
    $('[data-toggle="popover"]').popover()

    $('.select2').select2({data:select2_data});

    $('.select2').on('change', function () {
        $('#bencana-filter').submit();
    })

    //Date range picker
    $('#inputTanggalDiv').datetimepicker({
        format: 'D MMM YYYY',
        defaultDate: defaultDate,
    });

    $('.datepicker-input-group').datetimepicker({
        format: 'D MMM YYYY',
        defaultDate: new Date(),
    });

    $('#inputTanggalDiv').on("change.datetimepicker", function (date) {
        // console.log(moment(date.date._d).format('YYYY/M/DD'));
        $('#bencana-filter').submit();
    })
})