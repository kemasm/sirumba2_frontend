function open_gallery() {
    $("#map-card").removeClass("h-100");
    $("#map-card").addClass("h-75");
    $("#splide-card").show("slow");
    $('#splide-card .card-header .card-tools button').css('visibility', 'visible');
}

function close_gallery() {
    $("#splide-card").hide();
    $('#gallery-card').hide();
    $('#map-card').show();
    $("#map-card").removeClass("h-75");
    $("#map-card").addClass("h-100");
    $('#primary-slider .splide__track .splide__list').empty();
    $('#secondary-slider .splide__track .splide__list').empty();
}

function minimize_gallery() {
    $('#gallery-card').hide();
    $('#splide-card .card-header .card-tools button').css('visibility', 'visible');
    $('#map-card').show();
}

function maximize_gallery() {
    $('#map-card').hide();
    $('#gallery-card').show();
    $('#splide-card .card-header .card-tools button').css('visibility', 'hidden');
}

function build_splide(marker_id) {
    $.get('api/slider.php', {marker_id: marker_id}, function (data) {
        var elements = ''
        var slides = ''
        data.forEach(function (item) {
            var content;
            if (item.tag === 'img') {
                content = '<img data-splide-lazy="' + item.src + '"> '
            } else {
                content = '<iframe src="' + item.src + '"></iframe>'
            }
            elements += '<div class="splide__slide d-flex align-items-center"> ' + content + '</div>'
            slides += '<li class="splide__slide">  <img data-splide-lazy="' + item.thumbnail + '"></li>'
        });

        $('#primary-slider .splide__track .splide__list').append(elements);
        $('#secondary-slider .splide__track .splide__list').append(slides);

        var secondarySlider = new Splide('#secondary-slider', {
            fixedWidth: 200,
            height: 120,
            gap: 10,
            cover: true,
            pagination: false,
            isNavigation: true,
            focus: 'center',
            rewind: true,
            perPage: 3,
            breakpoints: {
                '600': {
                    fixedWidth: 66,
                    height: 40,
                }
            },
            lazyLoad: 'nearby',
            preloadPages: 1,
        }).mount();

        var primarySlider = new Splide('#primary-slider', {
            type: 'fade',
            height: 525.35,
            autowidth: true,
            pagination: false,
            arrows: false,
            drag: false,
            perPage: 1,
            lazyLoad: 'nearby',
            preloadPages: 1,
        });

        primarySlider.sync(secondarySlider).mount();
    })
}

// -------------------------------------------------------------------------------------------------------------
// gallery scripts
// -------------------------------------------------------------------------------------------------------------
$(function () {

    $('#gallery-card').hide();

    var minimize_gallery_button = $('#gallery-card .card-header .card-tools button.minimize-button');
    minimize_gallery_button.on('click', function () {
        minimize_gallery();
    });

    var close_gallery_button = $('#gallery-card .card-header .card-tools button.close-button, #splide-card .card-header .card-tools button');
    close_gallery_button.on('click', function () {
        close_gallery();
    });

    $('#secondary-slider').on('click', function () {
        maximize_gallery();
    });
})

// -------------------------------------------------------------------------------------------------------------
// leaflet scripts
// -------------------------------------------------------------------------------------------------------------
$(function () {
    $.ajax({
        type: 'GET',
        url: 'api/peta_bencana.php',
        async: false,
        data: {
            bencana_id: bencana_id,
        },
        success: function (response) {
            leaflet_data = response;
        }
    })

    var openstreetmap = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    });

    var map = L.map('main-map', {
        center: leaflet_data.center,
        zoom: 10,
        layers: [openstreetmap],
        zoomControl: false
    });

    var cities = L.layerGroup();

    $.each(leaflet_data.markers, function (index, value) {
        var marker = L.marker(value.latLng);
        marker.addTo(cities);

        marker.on('click', function () {
            close_gallery();
            build_splide(value.id);
            open_gallery();
        });
    })

    var baseLayers = {
        "Openstreetmap": openstreetmap,
    };

    var overlays = {
        "Titik Bencana": cities.addTo(map)
    };

    L.control.layers(baseLayers, overlays).addTo(map);

    var crosshairIcon = L.icon({
        iconUrl: 'static/frontend/dist/img/crosshair2.png',
        iconSize: [50, 50], // size of the icon
        iconAnchor: [10, 10], // point of the icon which will correspond to marker's location
    });
    var crosshair = new L.marker(map.getCenter(), {icon: crosshairIcon});
    var show_crosshair_button = L.easyButton({
        states: [{                 // specify different icons and responses for your button
            stateName: 'show-crosshair',
            onClick: function (btn, map) {
                // Add in a crosshair for the map
                crosshair.addTo(map);

                // Move the crosshair to the center of the map when the user pans
                map.on('move', function (e) {
                    crosshair.setLatLng(map.getCenter());
                });
                btn.state('hide-crosshair')
                add_marker_button.enable();
                btn.button.style.backgroundColor = 'red';
            },
            title: 'add new marker',
            icon: 'fa-map-pin'
        },{
            stateName: 'hide-crosshair',
            onClick: function (btn, map) {
                map.removeLayer(crosshair);
                btn.state('show-crosshair')
                add_marker_button.disable();
                btn.button.style.backgroundColor = '#28a745';
            },
            title: 'cancel add new marker',
            icon: 'fa-times'
        }]
    });
    show_crosshair_button.button.style.width = '44px';
    show_crosshair_button.button.style.height = '44px';
    show_crosshair_button.button.style.fontSize = '22px';
    show_crosshair_button.button.style.backgroundColor = '#28a745';
    show_crosshair_button.button.style.color = 'white';
    show_crosshair_button.button.style.padding = '7px';

    var add_marker_button = L.easyButton({
        states: [{
            stateName: 'add-marker',
            onClick: function (btn, map) {
                var center = map.getCenter();
                $.ajax({
                    type: "POST",
                    url: 'form/insert_form_marker.php',
                    data: {
                        'lat': center.lat,
                        'lng': center.lng,
                        'id-bencana': bencana_id,
                        'tanggal': currentDate,
                    },
                    success: function (data) {
                        if(!alert(data['message'])) {
                            var marker = L.marker(center);
                            marker.addTo(cities);
                            marker.on('click', function () {
                                close_gallery();
                                build_splide(data.id);
                                open_gallery();
                            });
                            map.removeLayer(crosshair);
                            show_crosshair_button.state('show-crosshair')
                            add_marker_button.disable();
                            show_crosshair_button.button.style.backgroundColor = '#28a745';
                        }
                    },
                    error: function (data) {
                        alert(data['message']);
                    }
                });
            },
            title: 'save new marker',
            icon: 'fa-save'
        }]
    });
    add_marker_button.button.style.width = '44px';
    add_marker_button.button.style.height = '44px';
    add_marker_button.button.style.fontSize = '22px';
    add_marker_button.button.style.backgroundColor = '#28a745';
    add_marker_button.button.style.color = 'white';
    add_marker_button.button.style.padding = '7px';
    add_marker_button.disable();

    var add_marker_bar = L.easyBar([show_crosshair_button, add_marker_button]);

    var zoom_button = L.control.zoom({position: 'bottomright'});
    var home_button = L.easyButton({
        position: 'bottomright',
        states: [{                 // specify different icons and responses for your button
            stateName: 'get-center',
            onClick: function (btn, map) {
                map.setView(leaflet_data.center, 10);
            },
            title: 'Zoom To Home',
            icon: 'fa-home'
        }]
    });

    zoom_button.addTo(map);
    home_button.addTo(map);
    add_marker_bar.addTo(map);
})