<?php
include("api/conn.php");

$balai_id = '';
$date = '';
$bencana_id = '';
$bencana_info = '';

// get balai id
if (isset($_GET['balai_id'])) {
  $balai_id = $_GET['balai_id'];
  $select2_sql = "SELECT mslink as id, tanggal as updated_date FROM balai WHERE mslink = {$balai_id} ORDER BY tanggal DESC, mslink DESC LIMIT 1";
} else {
  $select2_sql = "SELECT mslink as id, tanggal as updated_date FROM balai ORDER BY tanggal DESC, mslink DESC LIMIT 1";
};
$select2_sql_result = $conn->query($select2_sql);
$r = mysqli_fetch_assoc($select2_sql_result);
if (!isset($r)) {
  $error_message = "Balai tidak ditemukan, silahkan pilih balai lain";
  header("Location: 404.php?message=" . $error_message);
}
$balai_id = $r['id'];

// get date
$current_date = date("Y-m-d");
$date = $current_date;
if (isset($_GET['date'])) {
  $date = $_GET['date'];
}

// get bencana
//$bencana_sql = "SELECT * FROM bencana WHERE balai = {$balai_id} AND tanggal <= '{$date}' ORDER BY tanggal DESC, mslink DESC LIMIT 1 ";
$bencana_sql = "SELECT * FROM bencana WHERE balai = {$balai_id} ORDER BY mslink DESC LIMIT 1 ";
$bencana_sql_result = $conn->query($bencana_sql);
$r = mysqli_fetch_assoc($bencana_sql_result);
if (!isset($r)) {
  $error_message = 'Balai terkait belum melaporkan bencana. Silahkan pilih balai lain';
  header("Location: 404.php?message=" . $error_message);
}
$bencana_id = $r['mslink'];
$bencana_info = $r['ket'];

// info rumah
$sql = "SELECT *, SUM(berat + sedang + ringan + hancur + hilang) AS total FROM rumah_terdampak WHERE kode_bencana = {$bencana_id} AND tanggal <= '{$date}' GROUP BY mslink ORDER BY tanggal DESC, mslink DESC LIMIT 1 ";
$sql_result = $conn->query($sql);
$info_rumah = mysqli_fetch_assoc($sql_result);
if (!isset($info_rumah)) {
  $info_rumah = array("berat" => 0, "sedang" => 0, "ringan" => 0, "hancur" => 0, "hilang" => 0, "total" => 0, "tanggal" => "");
}

// info korban
$sql = "SELECT *, SUM(meninggal + luka + hilang + mengungsi) AS total FROM korban WHERE kode_bencana = {$bencana_id} AND tanggal <= '{$date}' GROUP BY mslink ORDER BY tanggal DESC, mslink DESC LIMIT 1";
$sql_result = $conn->query($sql);
$info_korban = mysqli_fetch_assoc($sql_result);
if (!isset($info_korban)) {
  $info_korban = array("meninggal" => 0, "luka" => 0, "hilang" => 0, "mengungsi" => 0, "total" => 0, "tanggal" => "");
}

/*
 * text cards contents
 */
$sql = "SELECT * FROM daya WHERE kode_bencana = {$bencana_id} AND tanggal <= '{$date}' ORDER BY tanggal DESC, mslink DESC LIMIT 1";
$sql_result = $conn->query($sql);
$daya = mysqli_fetch_assoc($sql_result);
if (!isset($daya)) {
  $daya = array("tanggal" => '', "ket" => "informasi sumber daya belum dimasukkan");
}

$sql = "SELECT * FROM kebutuhan WHERE kode_bencana = {$bencana_id} AND tanggal <= '{$date}' ORDER BY tanggal DESC, mslink DESC LIMIT 1";
$sql_result = $conn->query($sql);
$kebutuhan = mysqli_fetch_assoc($sql_result);
if (!isset($kebutuhan)) {
  $kebutuhan = array("tanggal" => '', "ket" => "informasi kebutuhan belum dimasukkan");
}

$sql = "SELECT * FROM kendala WHERE kode_bencana = {$bencana_id} AND tanggal <= '{$date}' ORDER BY tanggal DESC, mslink DESC LIMIT 1";
$sql_result = $conn->query($sql);
$kendala = mysqli_fetch_assoc($sql_result);
if (!isset($kendala)) {
  $kendala = array("tanggal" => '', "ket" => "informasi kendala belum dimasukkan");
}

$sql = "SELECT * FROM upaya WHERE kode_bencana = {$bencana_id} AND tanggal <= '{$date}' ORDER BY tanggal DESC, mslink DESC LIMIT 1";
$sql_result = $conn->query($sql);
$upaya = mysqli_fetch_assoc($sql_result);
if (!isset($upaya)) {
  $upaya = array("tanggal" => '', "ket" => "informasi upaya belum dimasukkan");
}

/*
 * graphs contents
 */
$pie_chart_data = array(
  'labels'=>array('Meninggal', 'Luka', 'Hilang', 'Mengungsi'),
  'data'=>array($info_korban['meninggal'], $info_korban['luka'], $info_korban['hilang'], $info_korban['mengungsi'])
);

$bar_chart_data = array(
  'labels'=>array('Rusak Ringan', 'Rusak Sedang', 'Rusak Berat', 'Hancur', 'Hilang'),
  'data'=>array($info_rumah['ringan'], $info_rumah['sedang'], $info_rumah['berat'], $info_rumah['hancur'], $info_rumah['hilang'])
);

$chart_status_rumah_data = array(
  'labels'=>array('Milik Sendiri', 'Bukan Milik Sendiri', 'Sewa/Kontrak'),
  'data'=>array(30, 50, 41)
);

$chart_status_lahan_data = array(
  'labels'=>array('Milik Sendiri', 'Bukan Milik Sendiri', 'Milik Negara'),
  'data'=>array(30, 50, 41)
);

$chart_gender_data = array(
  'labels'=>array('Laki-Laki', 'Perempuan', 'Total'),
  'data'=>array(701, 500, 1200)
);
