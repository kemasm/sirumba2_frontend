-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 04, 2021 at 07:07 PM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 7.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lebak`
--

-- --------------------------------------------------------

--
-- Table structure for table `balai`
--

CREATE TABLE `balai` (
  `mslink` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `ket` varchar(200) NOT NULL,
  `x` double NOT NULL,
  `y` double NOT NULL,
  `tanggal` date DEFAULT '2000-01-01'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `balai`
--

INSERT INTO `balai` (`mslink`, `nama`, `ket`, `x`, `y`, `tanggal`) VALUES
(1, 'BP2P Sumatera I', '(lingkup aceh)', 96.5, 4.78, '2021-01-29'),
(2, 'BP2P Sumatera II', '(lingkup sumatera utara)', 99.14, 2.649, '2021-03-31'),
(3, 'BP2P Sumatera III', '(lingkup Sumatera Barat, Prov Riau, dan Prov Kepri)', 100.85, 0.78, '2000-01-01'),
(4, 'BP2P Sumatera IV', '(lingkup Prov Bengkulu dan Prov Jambi)', 102.63, -1.58, '2000-01-01'),
(5, 'BP2P Sumatera V', '(lingkup Prov Sumsel, Prov Bangka Belitung, dan Prov Lampung)', 104.75, -3.174, '2000-01-01'),
(6, 'BP2P Jawa I', '(Prov DKI Jakarta dan Prov Banten)', 106.63, -6.22, '2000-01-01'),
(7, 'BP2P Jawa II', '(prov Jabar)', 107.55, -7.069, '2000-01-01'),
(8, 'BP2P Jawa III', '(Prov Jateng dan DIY)', 110.51, -7.46, '2000-01-01'),
(9, 'BP2P Jawa IV', '(Prov Jatim dan Bali)', 113.513, -8.01, '2000-01-01'),
(10, 'BP2P Nusa Tenggara I', '(prov NTB)', 117.4, -8.69, '2000-01-01'),
(11, 'BP2P Nusa Tenggara II', '(prov NTT)', 121.1, -8.66, '2000-01-01'),
(12, 'BP2P Kalimantan I', '(Prov Kalbar dan Kalteng)', 112.5, -0.95, '2000-01-01'),
(13, 'BP2P Kalimantan II', '(Prov Kalsel, Kaltim, dan Kaltara)', 116.38, 0.82, '2000-01-01'),
(14, 'BP2P Sulawesi I', '(Prov Sulut dan Gorontalo)', 122.86, 0.733, '2000-01-01'),
(15, 'BP2P Sulawesi II', '(Prov Sulteng dan Sulbar)', 120.12, -2.23, '2021-01-14'),
(16, 'BP2P Sulawesi III', '(Prov Sulsel dan Sultra)', 120.08, -3.63, '2000-01-01'),
(17, 'BP2P Maluku', '(Prov Maluku dan Maluku Utara)', 127.51, -1.59, '2000-01-01'),
(18, 'BP2P Papua I', '( Prov Papua)', 137.79, -4.24, '2000-01-01'),
(19, 'BP2P Papua II', '(Prov Papua Barat)', 132.15, -1.4, '2000-01-01');

-- --------------------------------------------------------

--
-- Table structure for table `bencana`
--

CREATE TABLE `bencana` (
  `mslink` int(11) NOT NULL,
  `balai` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `ket` text NOT NULL,
  `x` double NOT NULL,
  `y` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bencana`
--

INSERT INTO `bencana` (`mslink`, `balai`, `tanggal`, `ket`, `x`, `y`) VALUES
(1, 15, '2021-01-14', 'Gempa Bumi Mamuju - Majene 6,2 SM - 14 Januari 2021 (2.690000 LS, 118.860000 BT)\r\n ', 118.863, -2.69),
(2, 1, '2021-01-29', 'Gempa Bumi di Aceh Lokasi ..... tanggal 2021  ', 0, 0),
(5, 2, '2021-03-31', ' Meaan', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `bencana_marker`
--

CREATE TABLE `bencana_marker` (
  `id` int(11) NOT NULL,
  `bencana_id` int(11) NOT NULL,
  `lng` double DEFAULT NULL,
  `lat` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `bencana_marker`
--

INSERT INTO `bencana_marker` (`id`, `bencana_id`, `lng`, `lat`) VALUES
(1, 1, 118.863, -2.69),
(2, 1, 118.963, -2.79),
(3, 1, 118.763, -2.59),
(4, 1, 118.963, -2.69);

-- --------------------------------------------------------

--
-- Table structure for table `bencana_marker_item`
--

CREATE TABLE `bencana_marker_item` (
  `id` int(11) NOT NULL,
  `tag` varchar(512) DEFAULT NULL,
  `thumbnail` varchar(512) DEFAULT NULL,
  `src` varchar(512) DEFAULT NULL,
  `bencana_marker_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `bencana_marker_item`
--

INSERT INTO `bencana_marker_item` (`id`, `tag`, `thumbnail`, `src`, `bencana_marker_id`) VALUES
(1, 'iframe', 'static/frontend/dist/img/ss-1.png', '//sirumba.online/pv/pv.htm?config=pv.json', 1),
(2, 'img', 'static/frontend/dist/img/photo1.png', 'static/frontend/dist/img/photo1.png', 1),
(3, 'img', 'static/frontend/dist/img/photo2.png', 'static/frontend/dist/img/photo2.png', 1),
(4, 'img', 'static/frontend/dist/img/photo4.jpg', 'static/frontend/dist/img/photo4.jpg', 1),
(5, 'iframe', 'static/frontend/dist/img/ss-1.png', '//sirumba.online/pv/index.php?id=3', 2),
(6, 'img', 'static/frontend/dist/img/photo1.png', 'static/frontend/dist/img/photo1.png', 2),
(7, 'img', 'static/frontend/dist/img/photo2.png', 'static/frontend/dist/img/photo2.png', 2),
(8, 'img', 'static/frontend/dist/img/photo3.jpg', 'static/frontend/dist/img/photo3.jpg', 2),
(9, 'iframe', 'https://img.youtube.com/vi/tgbNymZ7vqY/0.jpg', 'https://www.youtube.com/embed/tgbNymZ7vqY', 3),
(10, 'img', 'static/frontend/dist/img/photo4.jpg', 'static/frontend/dist/img/photo4.jpg', 3),
(11, 'iframe', 'static/frontend/dist/img/ss-1.png', '//sirumba.online/pv/index.php?id=4', 4),
(12, 'iframe', 'https://img.youtube.com/vi/tgbNymZ7vqY/0.jpg', 'https://www.youtube.com/embed/tgbNymZ7vqY', 4);

-- --------------------------------------------------------

--
-- Table structure for table `daya`
--

CREATE TABLE `daya` (
  `mslink` int(11) NOT NULL,
  `balai` int(11) NOT NULL,
  `kode_bencana` int(11) NOT NULL DEFAULT 1,
  `tanggal` date NOT NULL,
  `ket` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `daya`
--

INSERT INTO `daya` (`mslink`, `balai`, `kode_bencana`, `tanggal`, `ket`) VALUES
(1, 15, 1, '2021-01-18', '1. Relawan 20 Orang'),
(5, 2, 5, '2021-04-01', ' hjhjh 89');

-- --------------------------------------------------------

--
-- Table structure for table `kebutuhan`
--

CREATE TABLE `kebutuhan` (
  `mslink` int(11) NOT NULL,
  `balai` int(11) NOT NULL,
  `kode_bencana` int(11) NOT NULL DEFAULT 1,
  `tanggal` date NOT NULL,
  `ket` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kebutuhan`
--

INSERT INTO `kebutuhan` (`mslink`, `balai`, `kode_bencana`, `tanggal`, `ket`) VALUES
(1, 15, 1, '2021-01-14', '1. Makanan dan minuman<br>\r\n2. Tenda <br>'),
(2, 2, 5, '2021-04-01', 'beras<br>\r\nGula PAsir <br> ');

-- --------------------------------------------------------

--
-- Table structure for table `kendala`
--

CREATE TABLE `kendala` (
  `mslink` int(11) NOT NULL,
  `balai` int(11) NOT NULL,
  `kode_bencana` int(11) NOT NULL DEFAULT 1,
  `tanggal` date NOT NULL,
  `ket` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kendala`
--

INSERT INTO `kendala` (`mslink`, `balai`, `kode_bencana`, `tanggal`, `ket`) VALUES
(1, 15, 1, '2021-01-20', 'Jaringan listrik terputus'),
(2, 2, 5, '2021-04-01', '  Jauh<br>\r\nsepi');

-- --------------------------------------------------------

--
-- Table structure for table `korban`
--

CREATE TABLE `korban` (
  `mslink` int(11) NOT NULL,
  `balai` int(11) NOT NULL,
  `kode_bencana` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `meninggal` int(11) NOT NULL,
  `luka` int(11) NOT NULL,
  `hilang` int(11) NOT NULL,
  `mengungsi` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `korban`
--

INSERT INTO `korban` (`mslink`, `balai`, `kode_bencana`, `tanggal`, `meninggal`, `luka`, `hilang`, `mengungsi`) VALUES
(1, 15, 1, '2021-01-27', 200, 99, 50, 130),
(3, 1, 2, '2021-03-31', 12, 1, 0, 12);

-- --------------------------------------------------------

--
-- Table structure for table `pengguna`
--

CREATE TABLE `pengguna` (
  `mslink` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `pwd` varchar(50) NOT NULL,
  `balai` int(11) NOT NULL,
  `aktif` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pengguna`
--

INSERT INTO `pengguna` (`mslink`, `nama`, `pwd`, `balai`, `aktif`) VALUES
(101, 'SRB101', '38b3eff8baf56627478ec76a704e9b52', 1, 1),
(102, 'SRB102', 'ec8956637a99787bd197eacd77acce5e', 1, 1),
(103, 'SRB103', '6974ce5ac660610b44d9b9fed0ff9548', 1, 1),
(104, 'SRB104', 'c9e1074f5b3f9fc8ea15d152add07294', 1, 1),
(105, 'SRB105', '65b9eea6e1cc6bb9f0cd2a47751a186f', 1, 1),
(106, 'SRB106', 'f0935e4cd5920aa6c7c996a5ee53a70f', 1, 1),
(107, 'SRB107', 'a97da629b098b75c294dffdc3e463904', 1, 1),
(108, 'SRB108', 'a3c65c2974270fd093ee8a9bf8ae7d0b', 1, 1),
(109, 'SRB109', '2723d092b63885e0d7c260cc007e8b9d', 1, 1),
(110, 'SRB110', '5f93f983524def3dca464469d2cf9f3e', 1, 1),
(201, 'SRB201', '757b505cfd34c64c85ca5b5690ee5293', 2, 1),
(202, 'SRB202', '854d6fae5ee42911677c739ee1734486', 2, 1),
(203, 'SRB203', 'e2c0be24560d78c5e599c2a9c9d0bbd2', 2, 1),
(204, 'SRB204', '274ad4786c3abca69fa097b85867d9a4', 2, 1),
(205, 'SRB205', 'eae27d77ca20db309e056e3d2dcd7d69', 2, 1),
(206, 'SRB206', '7eabe3a1649ffa2b3ff8c02ebfd5659f', 2, 1),
(207, 'SRB207', '69adc1e107f7f7d035d7baf04342e1ca', 2, 1),
(208, 'SRB208', '091d584fced301b442654dd8c23b3fc9', 2, 1),
(209, 'SRB209', 'b1d10e7bafa4421218a51b1e1f1b0ba2', 2, 1),
(210, 'SRB210', '6f3ef77ac0e3619e98159e9b6febf557', 2, 1),
(301, 'SRB301', '34ed066df378efacc9b924ec161e7639', 3, 1),
(302, 'SRB302', '577bcc914f9e55d5e4e4f82f9f00e7d4', 3, 1),
(303, 'SRB303', '11b9842e0a271ff252c1903e7132cd68', 3, 1),
(304, 'SRB304', '37bc2f75bf1bcfe8450a1a41c200364c', 3, 1),
(305, 'SRB305', '496e05e1aea0a9c4655800e8a7b9ea28', 3, 1),
(306, 'SRB306', 'b2eb7349035754953b57a32e2841bda5', 3, 1),
(307, 'SRB307', '8e98d81f8217304975ccb23337bb5761', 3, 1),
(308, 'SRB308', 'a8c88a0055f636e4a163a5e3d16adab7', 3, 1),
(309, 'SRB309', 'eddea82ad2755b24c4e168c5fc2ebd40', 3, 1),
(310, 'SRB310', '06eb61b839a0cefee4967c67ccb099dc', 3, 1),
(401, 'SRB401', '816b112c6105b3ebd537828a39af4818', 4, 1),
(402, 'SRB402', '69cb3ea317a32c4e6143e665fdb20b14', 4, 1),
(403, 'SRB403', 'bbf94b34eb32268ada57a3be5062fe7d', 4, 1),
(404, 'SRB404', '4f4adcbf8c6f66dcfc8a3282ac2bf10a', 4, 1),
(405, 'SRB405', 'bbcbff5c1f1ded46c25d28119a85c6c2', 4, 1),
(406, 'SRB406', '8cb22bdd0b7ba1ab13d742e22eed8da2', 4, 1),
(407, 'SRB407', 'f4f6dce2f3a0f9dada0c2b5b66452017', 4, 1),
(408, 'SRB408', '0d0fd7c6e093f7b804fa0150b875b868', 4, 1),
(409, 'SRB409', 'a96b65a721e561e1e3de768ac819ffbb', 4, 1),
(410, 'SRB410', '1068c6e4c8051cfd4e9ea8072e3189e2', 4, 1),
(501, 'SRB501', '5b69b9cb83065d403869739ae7f0995e', 5, 1),
(502, 'SRB502', 'b5b41fac0361d157d9673ecb926af5ae', 5, 1),
(503, 'SRB503', '285e19f20beded7d215102b49d5c09a0', 5, 1),
(504, 'SRB504', 'b337e84de8752b27eda3a12363109e80', 5, 1),
(505, 'SRB505', 'e8c0653fea13f91bf3c48159f7c24f78', 5, 1),
(506, 'SRB506', 'ff4d5fbbafdf976cfdc032e3bde78de5', 5, 1),
(507, 'SRB507', '2d6cc4b2d139a53512fb8cbb3086ae2e', 5, 1),
(508, 'SRB508', '389bc7bb1e1c2a5e7e147703232a88f6', 5, 1),
(509, 'SRB509', 'e2230b853516e7b05d79744fbd4c9c13', 5, 1),
(510, 'SRB510', '087408522c31eeb1f982bc0eaf81d35f', 5, 1),
(601, 'SRB601', 'b2f627fff19fda463cb386442eac2b3d', 6, 1),
(602, 'SRB602', 'c3992e9a68c5ae12bd18488bc579b30d', 6, 1),
(603, 'SRB603', 'd86ea612dec96096c5e0fcc8dd42ab6d', 6, 1),
(604, 'SRB604', '9cf81d8026a9018052c429cc4e56739b', 6, 1),
(605, 'SRB605', 'c361bc7b2c033a83d663b8d9fb4be56e', 6, 1),
(606, 'SRB606', '44c4c17332cace2124a1a836d9fc4b6f', 6, 1),
(607, 'SRB607', 'dc82d632c9fcecb0778afbc7924494a6', 6, 1),
(608, 'SRB608', '996a7fa078cc36c46d02f9af3bef918b', 6, 1),
(609, 'SRB609', 'd7a728a67d909e714c0774e22cb806f2', 6, 1),
(610, 'SRB610', '00ac8ed3b4327bdd4ebbebcb2ba10a00', 6, 1),
(701, 'SRB701', 'b4a528955b84f584974e92d025a75d1f', 7, 1),
(702, 'SRB702', 'b1eec33c726a60554bc78518d5f9b32c', 7, 1),
(703, 'SRB703', 'd6c651ddcd97183b2e40bc464231c962', 7, 1),
(704, 'SRB704', 'f64eac11f2cd8f0efa196f8ad173178e', 7, 1),
(705, 'SRB705', '4a47d2983c8bd392b120b627e0e1cab4', 7, 1),
(706, 'SRB706', '9c82c7143c102b71c593d98d96093fde', 7, 1),
(707, 'SRB707', '500e75a036dc2d7d2fec5da1b71d36cc', 7, 1),
(708, 'SRB708', 'ae0eb3eed39d2bcef4622b2499a05fe6', 7, 1),
(709, 'SRB709', '1ecfb463472ec9115b10c292ef8bc986', 7, 1),
(710, 'SRB710', 'e70611883d2760c8bbafb4acb29e3446', 7, 1),
(801, 'SRB801', '1905aedab9bf2477edc068a355bba31a', 8, 1),
(802, 'SRB802', '1141938ba2c2b13f5505d7c424ebae5f', 8, 1),
(803, 'SRB803', '1aa48fc4880bb0c9b8a3bf979d3b917e', 8, 1),
(804, 'SRB804', 'dc5689792e08eb2e219dce49e64c885b', 8, 1),
(805, 'SRB805', '846c260d715e5b854ffad5f70a516c88', 8, 1),
(806, 'SRB806', 'd58072be2820e8682c0a27c0518e805e', 8, 1),
(807, 'SRB807', '6e7b33fdea3adc80ebd648fffb665bb8', 8, 1),
(808, 'SRB808', 'a8ecbabae151abacba7dbde04f761c37', 8, 1),
(809, 'SRB809', '32b30a250abd6331e03a2a1f16466346', 8, 1),
(810, 'SRB810', 'b6edc1cd1f36e45daf6d7824d7bb2283', 8, 1),
(901, 'SRB901', '892c91e0a653ba19df81a90f89d99bcd', 9, 1),
(902, 'SRB902', 'b6a1085a27ab7bff7550f8a3bd017df8', 9, 1),
(903, 'SRB903', 'aa169b49b583a2b5af89203c2b78c67c', 9, 1),
(904, 'SRB904', 'f47d0ad31c4c49061b9e505593e3db98', 9, 1),
(905, 'SRB905', 'f57a2f557b098c43f11ab969efe1504b', 9, 1),
(906, 'SRB906', 'c8fbbc86abe8bd6a5eb6a3b4d0411301', 9, 1),
(907, 'SRB907', '621461af90cadfdaf0e8d4cc25129f91', 9, 1),
(908, 'SRB908', '8b6dd7db9af49e67306feb59a8bdc52c', 9, 1),
(909, 'SRB909', 'a4300b002bcfb71f291dac175d52df94', 9, 1),
(910, 'SRB910', 'e205ee2a5de471a70c1fd1b46033a75f', 9, 1),
(1001, 'SRB1001', 'b8c37e33defde51cf91e1e03e51657da', 10, 1),
(1002, 'SRB1002', 'fba9d88164f3e2d9109ee770223212a0', 10, 1),
(1003, 'SRB1003', 'aa68c75c4a77c87f97fb686b2f068676', 10, 1),
(1004, 'SRB1004', 'fed33392d3a48aa149a87a38b875ba4a', 10, 1),
(1005, 'SRB1005', '2387337ba1e0b0249ba90f55b2ba2521', 10, 1),
(1006, 'SRB1006', '9246444d94f081e3549803b928260f56', 10, 1),
(1007, 'SRB1007', 'd7322ed717dedf1eb4e6e52a37ea7bcd', 10, 1),
(1008, 'SRB1008', '1587965fb4d4b5afe8428a4a024feb0d', 10, 1),
(1009, 'SRB1009', '31b3b31a1c2f8a370206f111127c0dbd', 10, 1),
(1010, 'SRB1010', '1e48c4420b7073bc11916c6c1de226bb', 10, 1),
(1101, 'SRB1101', 'c6bff625bdb0393992c9d4db0c6bbe45', 11, 1),
(1102, 'SRB1102', 'c667d53acd899a97a85de0c201ba99be', 11, 1),
(1103, 'SRB1103', 'aace49c7d80767cffec0e513ae886df0', 11, 1),
(1104, 'SRB1104', '4da04049a062f5adfe81b67dd755cecc', 11, 1),
(1105, 'SRB1105', 'af21d0c97db2e27e13572cbf59eb343d', 11, 1),
(1106, 'SRB1106', 'c9f95a0a5af052bffce5c89917335f67', 11, 1),
(1107, 'SRB1107', 'e58cc5ca94270acaceed13bc82dfedf7', 11, 1),
(1108, 'SRB1108', 'b9d487a30398d42ecff55c228ed5652b', 11, 1),
(1109, 'SRB1109', '8f1d43620bc6bb580df6e80b0dc05c48', 11, 1),
(1110, 'SRB1110', '2cbca44843a864533ec05b321ae1f9d1', 11, 1),
(1201, 'SRB1201', '7501e5d4da87ac39d782741cd794002d', 12, 1),
(1202, 'SRB1202', '147702db07145348245dc5a2f2fe5683', 12, 1),
(1203, 'SRB1203', '491442df5f88c6aa018e86dac21d3606', 12, 1),
(1204, 'SRB1204', 'fb2fcd534b0ff3bbed73cc51df620323', 12, 1),
(1205, 'SRB1205', 'b571ecea16a9824023ee1af16897a582', 12, 1),
(1206, 'SRB1206', '144a3f71a03ab7c4f46f9656608efdb2', 12, 1),
(1207, 'SRB1207', '4e4e53aa080247bc31d0eb4e7aeb07a0', 12, 1),
(1208, 'SRB1208', 'a58149d355f02887dfbe55ebb2b64ba3', 12, 1),
(1209, 'SRB1209', '7e7e69ea3384874304911625ac34321c', 12, 1),
(1210, 'SRB1210', 'f7cade80b7cc92b991cf4d2806d6bd78', 12, 1),
(1301, 'SRB1301', '2df45244f09369e16ea3f9117ca45157', 13, 1),
(1302, 'SRB1302', '996009f2374006606f4c0b0fda878af1', 13, 1),
(1303, 'SRB1303', 'd282ef263719ab842e05382dc235f69e', 13, 1),
(1304, 'SRB1304', '5caf41d62364d5b41a893adc1a9dd5d4', 13, 1),
(1305, 'SRB1305', 'fd5c905bcd8c3348ad1b35d7231ee2b1', 13, 1),
(1306, 'SRB1306', '7940ab47468396569a906f75ff3f20ef', 13, 1),
(1307, 'SRB1307', 'f93882cbd8fc7fb794c1011d63be6fb6', 13, 1),
(1308, 'SRB1308', 'a0872cc5b5ca4cc25076f3d868e1bdf8', 13, 1),
(1309, 'SRB1309', '4476b929e30dd0c4e8bdbcc82c6ba23a', 13, 1),
(1310, 'SRB1310', '535ab76633d94208236a2e829ea6d888', 13, 1),
(1401, 'SRB1401', '9701a1c165dd9420816bfec5edd6c2b1', 14, 1),
(1402, 'SRB1402', '28fc2782ea7ef51c1104ccf7b9bea13d', 14, 1),
(1403, 'SRB1403', '4edaa105d5f53590338791951e38c3ad', 14, 1),
(1404, 'SRB1404', '186a157b2992e7daed3677ce8e9fe40f', 14, 1),
(1405, 'SRB1405', '3de2334a314a7a72721f1f74a6cb4cee', 14, 1),
(1406, 'SRB1406', 'b7087c1f4f89e63af8d46f3b20271153', 14, 1),
(1407, 'SRB1407', '46771d1f432b42343f56f791422a4991', 14, 1),
(1408, 'SRB1408', '6d9c547cf146054a5a720606a7694467', 14, 1),
(1409, 'SRB1409', '7b5b23f4aadf9513306bcd59afb6e4c9', 14, 1),
(1410, 'SRB1410', '512c5cad6c37edb98ae91c8a76c3a291', 14, 1),
(1501, 'SRB1501', '5cbdfd0dfa22a3fca7266376887f549b', 15, 1),
(1502, 'SRB1502', '77f959f119f4fb2321e9ce801e2f5163', 15, 1),
(1503, 'SRB1503', '0c8ce55163055c4da50a81e0a273468c', 15, 1),
(1504, 'SRB1504', '49b8b4f95f02e055801da3b4f58e28b7', 15, 1),
(1505, 'SRB1505', '0c9ebb2ded806d7ffda75cd0b95eb70c', 15, 1),
(1506, 'SRB1506', 'b5488aeff42889188d03c9895255cecc', 15, 1),
(1507, 'SRB1507', '4dcf435435894a4d0972046fc566af76', 15, 1),
(1508, 'SRB1508', 'ebd6d2f5d60ff9afaeda1a81fc53e2d0', 15, 1),
(1509, 'SRB1509', '1cd3882394520876dc88d1472aa2a93f', 15, 1),
(1510, 'SRB1510', '41a60377ba920919939d83326ebee5a1', 15, 1),
(1601, 'SRB1601', 'c559da2ba967eb820766939a658022c8', 16, 1),
(1602, 'SRB1602', '1bc0249a6412ef49b07fe6f62e6dc8de', 16, 1),
(1603, 'SRB1603', 'f3173935ed8ac4bf073c1bcd63171f8a', 16, 1),
(1604, 'SRB1604', 'a368b0de8b91cfb3f91892fbf1ebd4b2', 16, 1),
(1605, 'SRB1605', 'f18a6d1cde4b205199de8729a6637b42', 16, 1),
(1606, 'SRB1606', '495dabfd0ca768a3c3abd672079f48b6', 16, 1),
(1607, 'SRB1607', '9597353e41e6957b5e7aa79214fcb256', 16, 1),
(1608, 'SRB1608', 'faafda66202d234463057972460c04f5', 16, 1),
(1609, 'SRB1609', 'a2cc63e065705fe938a4dda49092966f', 16, 1),
(1610, 'SRB1610', 'a14ac55a4f27472c5d894ec1c3c743d2', 16, 1),
(1701, 'SRB1701', '15231a7ce4ba789d13b722cc5c955834', 17, 1),
(1702, 'SRB1702', 'b9f94c77652c9a76fc8a442748cd54bd', 17, 1),
(1703, 'SRB1703', '375c71349b295fbe2dcdca9206f20a06', 17, 1),
(1704, 'SRB1704', 'a588a6199feff5ba48402883d9b72700', 17, 1),
(1705, 'SRB1705', '2a27b8144ac02f67687f76782a3b5d8f', 17, 1),
(1706, 'SRB1706', '8bb88f80d334b1869781beb89f7b73be', 17, 1),
(1707, 'SRB1707', '02f039058bd48307e6f653a2005c9dd2', 17, 1),
(1708, 'SRB1708', 'c59b469d724f7919b7d35514184fdc0f', 17, 1),
(1709, 'SRB1709', '52d080a3e172c33fd6886a37e7288491', 17, 1),
(1710, 'SRB1710', '5a142a55461d5fef016acfb927fee0bd', 17, 1),
(1801, 'SRB1801', 'cd14821dab219ea06e2fd1a2df2e3582', 18, 1),
(1802, 'SRB1802', 'dc40b7120e77741d191c0d2b82cea7be', 18, 1),
(1803, 'SRB1803', '3fab5890d8113d0b5a4178201dc842ad', 18, 1),
(1804, 'SRB1804', '90e1357833654983612fb05e3ec9148c', 18, 1),
(1805, 'SRB1805', '7ffd85d93a3e4de5c490d304ccd9f864', 18, 1),
(1806, 'SRB1806', 'd1e946f4e67db4b362ad23818a6fb78a', 18, 1),
(1807, 'SRB1807', '1f1baa5b8edac74eb4eaa329f14a0361', 18, 1),
(1808, 'SRB1808', '39027dfad5138c9ca0c474d71db915c3', 18, 1),
(1809, 'SRB1809', '645098b086d2f9e1e0e939c27f9f2d6f', 18, 1),
(1810, 'SRB1810', '6e79ed05baec2754e25b4eac73a332d2', 18, 1),
(1901, 'SRB1901', 'd54e99a6c03704e95e6965532dec148b', 19, 1),
(1902, 'SRB1902', 'fc4ddc15f9f4b4b06ef7844d6bb53abf', 19, 1),
(1903, 'SRB1903', '944626adf9e3b76a3919b50dc0b080a4', 19, 1),
(1904, 'SRB1904', 'c91591a8d461c2869b9f535ded3e213e', 19, 1),
(1905, 'SRB1905', '73e0f7487b8e5297182c5a711d20bf26', 19, 1),
(1906, 'SRB1906', 'dea9ddb25cbf2352cf4dec30222a02a5', 19, 1),
(1907, 'SRB1907', '77369e37b2aa1404f416275183ab055f', 19, 1),
(1908, 'SRB1908', '65699726a3c601b9f31bf04019c8593c', 19, 1),
(1909, 'SRB1909', '0609154fa35b3194026346c9cac2a248', 19, 1),
(1910, 'SRB1910', 'ab7314887865c4265e896c6e209d1cd6', 19, 1);

-- --------------------------------------------------------

--
-- Table structure for table `rumah_terdampak`
--

CREATE TABLE `rumah_terdampak` (
  `mslink` int(11) NOT NULL,
  `balai` int(11) NOT NULL,
  `kode_bencana` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `berat` int(11) NOT NULL,
  `sedang` int(11) NOT NULL,
  `ringan` int(11) NOT NULL,
  `hancur` int(11) NOT NULL,
  `hilang` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rumah_terdampak`
--

INSERT INTO `rumah_terdampak` (`mslink`, `balai`, `kode_bencana`, `tanggal`, `berat`, `sedang`, `ringan`, `hancur`, `hilang`) VALUES
(1, 15, 1, '2021-01-27', 3, 10, 134, 1, 20),
(2, 1, 2, '2021-03-31', 23, 45, 2, 10, 20);

-- --------------------------------------------------------

--
-- Table structure for table `upaya`
--

CREATE TABLE `upaya` (
  `mslink` int(11) NOT NULL,
  `balai` int(11) NOT NULL,
  `kode_bencana` int(11) NOT NULL DEFAULT 1,
  `tanggal` date NOT NULL,
  `ket` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `upaya`
--

INSERT INTO `upaya` (`mslink`, `balai`, `kode_bencana`, `tanggal`, `ket`) VALUES
(1, 15, 1, '2021-01-20', '1. Konsolidasi<br>\r\n'),
(3, 1, 2, '2021-03-31', ' 1. iii <br>\r\n2.ioio<br>\r\n '),
(4, 2, 5, '2021-03-30', 'Supaya <br>\r\nbaik Jalannua<br>');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bencana`
--
ALTER TABLE `bencana`
  ADD PRIMARY KEY (`mslink`);

--
-- Indexes for table `bencana_marker`
--
ALTER TABLE `bencana_marker`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bencana_id` (`bencana_id`);

--
-- Indexes for table `bencana_marker_item`
--
ALTER TABLE `bencana_marker_item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bencana_marker_id` (`bencana_marker_id`);

--
-- Indexes for table `daya`
--
ALTER TABLE `daya`
  ADD PRIMARY KEY (`mslink`);

--
-- Indexes for table `kebutuhan`
--
ALTER TABLE `kebutuhan`
  ADD PRIMARY KEY (`mslink`);

--
-- Indexes for table `kendala`
--
ALTER TABLE `kendala`
  ADD PRIMARY KEY (`mslink`);

--
-- Indexes for table `korban`
--
ALTER TABLE `korban`
  ADD PRIMARY KEY (`mslink`);

--
-- Indexes for table `pengguna`
--
ALTER TABLE `pengguna`
  ADD PRIMARY KEY (`mslink`);

--
-- Indexes for table `rumah_terdampak`
--
ALTER TABLE `rumah_terdampak`
  ADD PRIMARY KEY (`mslink`);

--
-- Indexes for table `upaya`
--
ALTER TABLE `upaya`
  ADD PRIMARY KEY (`mslink`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bencana`
--
ALTER TABLE `bencana`
  MODIFY `mslink` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `bencana_marker`
--
ALTER TABLE `bencana_marker`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `bencana_marker_item`
--
ALTER TABLE `bencana_marker_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `daya`
--
ALTER TABLE `daya`
  MODIFY `mslink` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `kebutuhan`
--
ALTER TABLE `kebutuhan`
  MODIFY `mslink` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `kendala`
--
ALTER TABLE `kendala`
  MODIFY `mslink` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `korban`
--
ALTER TABLE `korban`
  MODIFY `mslink` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `pengguna`
--
ALTER TABLE `pengguna`
  MODIFY `mslink` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1911;

--
-- AUTO_INCREMENT for table `rumah_terdampak`
--
ALTER TABLE `rumah_terdampak`
  MODIFY `mslink` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `upaya`
--
ALTER TABLE `upaya`
  MODIFY `mslink` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `bencana_marker`
--
ALTER TABLE `bencana_marker`
  ADD CONSTRAINT `bencana_marker_ibfk_1` FOREIGN KEY (`bencana_id`) REFERENCES `bencana` (`mslink`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `bencana_marker_item`
--
ALTER TABLE `bencana_marker_item`
  ADD CONSTRAINT `bencana_marker_item_ibfk_1` FOREIGN KEY (`bencana_marker_id`) REFERENCES `bencana_marker` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
