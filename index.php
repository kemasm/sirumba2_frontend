<?php

session_start();

if (empty($_SESSION["SRB2MSLINK"])) {
  $SRB2MSLINK = 0;
  $SRB2NAMA = "";
  $SRB2BALAI = 0;
} else {
  $SRB2MSLINK = $_SESSION["SRB2MSLINK"];
  $SRB2NAMA = $_SESSION["SRB2NAMA"];
  $SRB2BALAI = $_SESSION["SRB2BALAI"];
}

?>

<?php include 'initiate_data.php' ?>

<!DOCTYPE html>
<html lang="en">

<?php include 'head.php' ?>

<body class="hold-transition layout-top-nav">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand-md navbar-light navbar-white px-4 bg-peach m-0">
    <!-- <div class="container"> -->
    <a href="" class="navbar-brand d-flex align-items-center">
      <img src="static/frontend/dist/img/220px-Logo_PU_(RGB).jpg" alt="" class="brand-image" style="opacity: .8"
           height="33">
      <span class="brand-text font-weight-normal h1 mb-0">
          SIRUMBA <small class="d-none d-md-inline ">Sistem Informasi Rumah Terdampak</small>
        </span>
    </a>

    <!-- Right navbar links -->
    <ul class="order-1 order-md-3 navbar-nav navbar-no-expand ml-auto">

      <li class="nav-item">
        <?php if ($SRB2BALAI == 0) { ?>
          <button class="btn btn-block btn-secondary" data-toggle="modal" data-target="#modal-login">
            Login
          </button>
        <?php } else { ?>
          <a class="btn btn-block btn-secondary" href='api/logout.php'>
            <?php echo $SRB2NAMA . " - Logout "; ?>
          </a>
        <?php } ?>
      </li>

    </ul>
    <!-- </div> -->
  </nav>
  <!-- /.navbar -->

  <!-- Navbar Filter-->
  <form id="bencana-filter" action="form/filter_form_bencana.php" method="POST"></form>
  <nav class="main-header navbar navbar-expand-md navbar-light bg-warning border-0 px-4 justify-content-between m-0">
    <!-- <div class="container"> -->
    <div class="form-group row mb-2">
      <label for="inputBalai" class="col-form-label col">Balai Wilayah</label>
      <div class=col-auto>
        <select id="inputBalai" class="form-control select2 select2-warning"
                data-dropdown-css-class="select2-warning" name="inputBalai"
                form="bencana-filter"
                style="width: 200px;">
        </select>
      </div>
    </div>

    <!-- Right navbar links -->

    <div class="form-group row mb-2 date" id="inputTanggalDiv" data-target-input="nearest">
      <label for="inputTanggal" class="col-form-label col">Tanggal</label>
      <div class="col-auto">
        <input id="inputTanggal" type="text" class="form-control datetimepicker-input"
               data-target="#inputTanggalDiv" name="inputTanggal" form="bencana-filter"
               data-toggle="datetimepicker" style="text-align: right; width: 200px"/>
      </div>
    </div>

    <!-- </div> -->
  </nav>
  <!-- /.navbar filter-->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper m-0">
    <!-- Content Header (Page header) -->
    <div class="content-header p-0">
      <div class="row">
        <div class="col-sm-12">
          <div class="border border-dark rounded-bottom mb-3">
            <div class="card-body">
              <h6 class="card-title">
<!--                Gempa Bumi Mamuju - Majene 6,2 SM - 14 Januari 2021 (2.990000 LS, 118.860000 BT)-->
                <?php echo $bencana_info; ?>
              </h6>
            </div>
          </div>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <!-- <div class="container"> -->
      <div class="row">
        <div class="col-lg-12">
          <div class="card card-info card-outline card-outline-tabs bg-peach shadow-none">

            <!-- nav-tabs -->
            <div class="card-header p-0 border-bottom-0">
              <ul class="nav nav-tabs" id="custom-tabs-four-tab" role="tablist">
                <li class="nav-item">
                  <a class="nav-link active" id="pendataan-cepat-tab" data-toggle="pill"
                     href="#pendataan-cepat"
                     role="tab" aria-controls="pendataan-cepat" aria-selected="true">
                    <strong>Pendataan Cepat</strong>
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="pendataan-mendalam-tab" data-toggle="pill"
                     href="#pendataan-mendalam"
                     role="tab" aria-controls="pendataan-mendalam" aria-selected="false">
                    <strong>Pendataan Mendalam</strong>
                  </a>
                </li>
              </ul>
            </div>

            <div class="card-body p-2">
              <div class="tab-content" id="custom-tabs-four-tabContent">

                <div class="tab-pane fade show active" id="pendataan-cepat" role="tabpanel"
                     aria-labelledby="pendataan-cepat-tab">

                  <div class="col-lg-12">

                    <!-- lokasi foto -->
                    <div class="card mb-0 bg-transparent shadow-none">

                      <div class="card-header pr-0 pl-0 border-0">
                        <h2 class="m-0" style="width: 100%;text-align: center;">
                          <strong>Lokasi & Foto Bencana</strong>
                        </h2>
                      </div>

                      <div class="card-body pr-0 pl-0">
                        <div class="row">

                          <div class="col-lg-8 col-sm-12" style="min-height: 90vh;">
                            <div class="row h-100">
                              <div class="col-sm-12 col-lg-12 mb-3">
                                <div id="map-card" class="card h-100 mb-0">
                                  <div class="card-body p-0">
                                    <div id="main-map" style="height: 100%;"></div>
                                  </div>
                                  <!-- /.card-body -->
                                </div>
                                <!-- /.card -->

                                <div id="gallery-card" class="card bg-dark h-75 mb-0"
                                     style="display: none">
                                  <div class="card-header p-1 border-0">
                                    <div class="card-tools m-0">
                                      <button type="button"
                                              class="btn btn-tool minimize-button">
                                        <i
                                            class="fas fa-minus"></i>
                                      </button>
                                      <button type="button"
                                              class="btn btn-tool close-button"><i
                                            class="fas fa-times"></i>
                                      </button>
                                    </div>
                                    <!-- /.card-tools -->
                                  </div>
                                  <!-- /.card-header -->
                                  <div class="card-body p-0">
                                    <div id="primary-slider" class="splide p-0">
                                      <div class="splide__track">
                                        <div class="splide__list w-100">
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <!-- /.card-body -->
                                </div>
                                <!-- /.card -->

                                <div id="splide-card" class="card bg-dark h-auto mb-0"
                                     style="display: none">
                                  <div class="card-header p-1 border-0">
                                    <div class="card-tools m-0">
                                      <button type="button" class="btn btn-tool">
                                        <i class="fas fa-times"></i>
                                      </button>
                                    </div>
                                    <!-- /.card-tools -->
                                  </div>
                                  <!-- /.card-header -->
                                  <div class="card-body pt-0">
                                    <div id="secondary-slider" class="splide py-0">
                                      <div class="splide__track">
                                        <ul class="splide__list">
                                        </ul>
                                      </div>
                                    </div>
                                  </div>
                                  <!-- /.card-body -->
                                </div>
                                <!-- /.card -->
                              </div>
                            </div>
                          </div>
                          <!-- /.col-lg-8 col-sm-12 -->

                          <div class="col-lg-4 col-sm-12">
                            <div class="row h-100">
                              <div class="col-sm-6 col-lg-12 mb-3">
                                <div class="card bg-danger-custom h-100">
                                  <div class="card-header pb-0"
                                       style="border-bottom: 0; min-height: 87.2px;">
                                    <div class="d-flex justify-content-between align-items-center h-100">
                                      <h3><strong>Total Korban</strong></h3>
                                      <h3><strong><?php echo $info_korban["total"] ?></strong></h3>
                                    </div>
                                  </div>
                                  <div class="card-body">
                                    <ul class="list-group list-group-flush"
                                        style="color: black; border-radius: 5px;">
                                      <li class="list-group-item">
                                        <div class="float-left">Meninggal</div>
                                        <div class="float-Right">
                                          <div class="text-right"><?php echo $info_korban["meninggal"] ?></div>
                                        </div>
                                      </li>
                                      <li class="list-group-item">
                                        <div class="float-left">Luka</div>
                                        <div class="float-Right">
                                          <div class="text-right"><?php echo $info_korban["luka"] ?></div>
                                        </div>
                                      </li>
                                      <li class="list-group-item">
                                        <div class="float-left">Hilang</div>
                                        <div class="float-Right">
                                          <div class="text-right"><?php echo $info_korban["hilang"] ?></div>
                                        </div>
                                      </li>
                                      <li class="list-group-item">
                                        <div class="float-left">Mengungsi</div>
                                        <div class="float-Right">
                                          <div class="text-right"><?php echo $info_korban["mengungsi"] ?></div>
                                        </div>
                                      </li>
                                    </ul>
                                  </div>
                                  <!-- /.card-body -->
                                </div>
                              </div>
                              <!-- /.card -->

                              <div class="col-sm-6 col-lg-12 mb-3">
                                <div class="card bg-warning-custom h-100">
                                  <div class="card-header pb-0"
                                       style="border-bottom: 0; min-height: 87.2px;">
                                    <div class="d-flex justify-content-between align-items-center h-100">
                                      <h3><strong>Total Rumah Terdampak</strong>
                                      </h3>
                                      <h3><strong><?php echo $info_rumah["total"] ?></strong></h3>
                                    </div>
                                  </div>
                                  <div class="card-body">
                                    <ul class="list-group list-group-flush"
                                        style="border-radius: 5px;">
                                      <li class="list-group-item">
                                        <div class="float-left">Rusak Berat
                                        </div>
                                        <div class="float-Right">
                                          <div class="text-right"><?php echo $info_rumah["berat"] ?></div>
                                        </div>
                                      </li>
                                      <li class="list-group-item">
                                        <div class="float-left">Rusak Sedang
                                        </div>
                                        <div class="float-Right">
                                          <div class="text-right"><?php echo $info_rumah["sedang"] ?></div>
                                        </div>
                                      </li>
                                      <li class="list-group-item">
                                        <div class="float-left">Rusak Ringan
                                        </div>
                                        <div class="float-Right">
                                          <div class="text-right"><?php echo $info_rumah["ringan"] ?></div>
                                        </div>
                                      </li>
                                      <li class="list-group-item">
                                        <div class="float-left">Hancur</div>
                                        <div class="float-Right">
                                          <div class="text-right"><?php echo $info_rumah["hancur"] ?></div>
                                        </div>
                                      </li>
                                      <li class="list-group-item">
                                        <div class="float-left">Hilang</div>
                                        <div class="float-Right">
                                          <div class="text-right"><?php echo $info_rumah["hilang"] ?></div>
                                        </div>
                                      </li>
                                    </ul>
                                  </div>
                                  <!-- /.card-body -->
                                </div>
                              </div>
                              <!-- /.card -->
                            </div>
                          </div>
                          <!-- /.col-lg-4 col-sm-12 -->

                        </div>
                      </div>

                    </div>
                    <!-- /lokasi foto -->

                    <!-- graphs -->
                    <div class="card mb-0 bg-transparent shadow-none">

                      <div class="card-header pr-0 pl-0 border-0">
                        <h2 class="m-0" style="width: 100%;text-align: center;">
                          <strong>Dampak Bencana</strong>
                        </h2>
                      </div>

                      <div class="card-body pr-0 pl-0">

                        <div class="row">

                          <div class="col-lg-6 col-md-6 mb-3">
                            <div class="card card-danger h-100 info-card">
                              <div class="card-header border-0">

                                <div class="d-flex justify-content-between align-items-center">
                                  <h3 class="card-title">
                                    <strong>Status Warga Terdampak</strong><br>
                                    <small>per tanggal 31 Desember 2020, 20.41
                                      PM</small>
                                  </h3>
                                  <div>
                                    <i class="fa fa-edit" aria-hidden="true">
                                    </i>
                                    <i class="fa fa-info-circle" aria-hidden="true"
                                       data-container="body"
                                       data-toggle="popover" data-placement="top"
                                       data-trigger="hover"
                                       data-content="Sumber data: Survey PU, 15 Jan 2021">
                                    </i>
                                  </div>
                                </div>
                              </div>

                              <div class="card-body">
                                <div class="graph-section">
                                  <canvas id="pieChart"
                                          style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                                </div>

                                <div class="form-section py-0">
                                  <form id="form-warga" method="POST" action="form/status_warga_form.php"></form>
                                  <div class="form-group row">
                                    <label for="input-meninggal" class="col-sm-4 col-form-label">Meninggal</label>
                                    <div class="col-sm-8">
                                      <input type="number" id="input-meninggal" class="form-control" name="meninggal" form="form-warga" required
                                             placeholder="Contoh: 10" value="<?php echo $info_korban['meninggal'] ?>">
                                    </div>
                                  </div>
                                  <div class="form-group row">
                                    <label for="input-luka" class="col-sm-4 col-form-label">Luka</label>
                                    <div class="col-sm-8">
                                      <input type="number" id="input-luka" class="form-control" name="luka" form="form-warga" required
                                             placeholder="Contoh: 10" value="<?php echo $info_korban['luka'] ?>">
                                    </div>
                                  </div>
                                  <div class="form-group row">
                                    <label for="input-hilang" class="col-sm-4 col-form-label">Hilang</label>
                                    <div class="col-sm-8">
                                      <input type="number" id="input-hilang" class="form-control" name="hilang" form="form-warga" required
                                             placeholder="Contoh: 10" value="<?php echo $info_korban['hilang'] ?>">
                                    </div>
                                  </div>
                                  <div class="form-group row">
                                    <label for="input-mengungsi" class="col-sm-4 col-form-label">Mengungsi</label>
                                    <div class="col-sm-8">
                                      <input type="number" id="input-mengungsi" class="form-control" name="mengungsi" form="form-warga" required
                                             placeholder="Contoh: 10" value="<?php echo $info_korban['mengungsi'] ?>">
                                    </div>
                                  </div>
                                  <div class="form-group row">
                                    <div class="input-group datepicker-input-group" id="fg-tanggal-status-warga" data-target-input="nearest">
                                      <label for="input-tanggal" class="col-sm-4 col-form-label">Tanggal</label>
                                      <div class="col-sm-8">
                                        <input name="tanggal" id="input-tanggal" type="text" class="form-control datetimepicker-input" required
                                               data-target="#fg-tanggal-status-warga" data-toggle="datetimepicker"
                                               form="form-warga" placeholder="Contoh: 7 Mar 2021"/>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="form-group row d-flex justify-content-end">
                                    <input name="id-bencana" form="form-warga" value="<?php echo $bencana_id ?>" hidden readonly>
                                    <button type="button" class="btn btn-default btn-cancel-form"><i class="fas fa-times"></i>
                                      Cancel
                                    </button>&nbsp;
                                    <button type="submit" class="btn btn-info" form="form-warga"><i class="fa fa-save"></i>
                                      Simpan
                                    </button>
                                  </div>
                                </div>

                                <script>
                                  $(function () {
                                      $('#form-warga').on('submit', function (e) {
                                          e.preventDefault();
                                          $.ajax({
                                              type: "POST",
                                              data : $(this).serialize(),
                                              url: "form/status_warga_form.php",
                                              success: function(data){
                                                  if(!alert(data['message'])) {window.location.reload();}
                                              },
                                              error: function (data) {
                                                  alert(data['message']);
                                              }
                                          });
                                      })
                                  })
                                </script>
                              </div>

                            </div>
                            <!-- /.card -->
                          </div>
                          <!-- /.col-md-6 -->

                          <div class="col-lg-6 col-md-6 mb-3">
                            <div class="card card-warning h-100 info-card">
                              <div class="card-header border-0">

                                <div class="d-flex justify-content-between align-items-center">
                                  <h3 class="card-title">
                                    <strong>Kerusakan Rumah Terdampak</strong><br>
                                    <small>per tanggal 31 Desember 2020, 20.41
                                      PM</small>
                                  </h3>
                                  <div>
                                    <i class="fa fa-edit" aria-hidden="true">
                                    </i>
                                    <i class="fa fa-info-circle" aria-hidden="true"
                                       data-container="body"
                                       data-toggle="popover" data-placement="top"
                                       data-trigger="hover"
                                       data-content="Sumber data: Survey PU, 15 Jan 2021">
                                    </i>
                                  </div>
                                </div>
                              </div>

                              <div class="card-body">
                                <div class="graph-section">
                                  <canvas id="barChart"
                                          style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                                </div>

                                <div class="form-section py-0">
                                  <form id="form-rumah" method="POST" action="form/rumah_terdampak_form.php"></form>
                                  <div class="form-group row">
                                    <label for="input-ringan" class="col-sm-4 col-form-label">Rusak Ringan</label>
                                    <div class="col-sm-8">
                                      <input type="number" id="input-ringan" class="form-control" name="ringan" form="form-rumah" required
                                             placeholder="Contoh: 10" value="<?php echo $info_rumah['ringan'] ?>">
                                    </div>
                                  </div>
                                  <div class="form-group row">
                                    <label for="input-sedang" class="col-sm-4 col-form-label">Rusak Sedang</label>
                                    <div class="col-sm-8">
                                      <input type="number" id="input-sedang" class="form-control" name="sedang" form="form-rumah" required
                                             placeholder="Contoh: 10" value="<?php echo $info_rumah['sedang'] ?>">
                                    </div>
                                  </div>
                                  <div class="form-group row">
                                    <label for="input-berat" class="col-sm-4 col-form-label">Rusak Berat</label>
                                    <div class="col-sm-8">
                                      <input type="number" id="input-berat" class="form-control" name="berat" form="form-rumah" required
                                             placeholder="Contoh: 10" value="<?php echo $info_rumah['berat'] ?>">
                                    </div>
                                  </div>
                                  <div class="form-group row">
                                    <label for="input-hancur" class="col-sm-4 col-form-label">Hancur</label>
                                    <div class="col-sm-8">
                                      <input type="number" id="input-hancur" class="form-control" name="hancur" form="form-rumah" required
                                             placeholder="Contoh: 10" value="<?php echo $info_rumah['hancur'] ?>">
                                    </div>
                                  </div>
                                  <div class="form-group row">
                                    <label for="input-hilang" class="col-sm-4 col-form-label">Hilang</label>
                                    <div class="col-sm-8">
                                      <input type="number" id="input-hilang" class="form-control" name="hilang" form="form-rumah" required
                                             placeholder="Contoh: 10" value="<?php echo $info_rumah['hilang'] ?>">
                                    </div>
                                  </div>
                                  <div class="form-group row">
                                    <div class="input-group datepicker-input-group" id="fg-tanggal-rumah-terdampak" data-target-input="nearest">
                                      <label for="input-tanggal-rumah" class="col-sm-4 col-form-label">Tanggal</label>
                                      <div class="col-sm-8">
                                        <input name="tanggal" id="input-tanggal-rumah" type="text" class="form-control datetimepicker-input" required
                                               data-target="#fg-tanggal-rumah-terdampak" data-toggle="datetimepicker"
                                               form="form-rumah" placeholder="Contoh: 7 Mar 2021"/>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="form-group row d-flex justify-content-end">
                                    <input name="id-bencana" form="form-rumah" value="<?php echo $bencana_id ?>" hidden readonly>
                                    <button type="button" class="btn btn-default btn-cancel-form"><i class="fas fa-times"></i>
                                      Cancel
                                    </button>&nbsp;
                                    <button type="submit" class="btn btn-info" form="form-rumah"><i class="fa fa-save"></i>
                                      Simpan
                                    </button>
                                  </div>
                                </div>

                                <script>
                                    $(function () {
                                        $('#form-rumah').on('submit', function (e) {
                                            e.preventDefault();
                                            $.ajax({
                                                type: "POST",
                                                data : $(this).serialize(),
                                                url: "form/rumah_terdampak_form.php",
                                                success: function(data){
                                                    if(!alert(data['message'])) {window.location.reload();}
                                                },
                                                error: function (data) {
                                                    alert(data['message']);
                                                }
                                            });
                                        })
                                    })
                                </script>
                              </div>

                            </div>
                            <!-- /.card -->
                          </div>
                          <!-- /.col-md-6 -->

                        </div>

                        <div class="row">

                          <div class="col-lg-6 col-md-6 mb-3">
                            <div class="card card-danger h-100 info-card">
                              <div class="card-header">
                                <div class="d-flex justify-content-between">
                                  <h5 class="card-title m-0"><strong>Upaya yang
                                      Dilakukan</strong></h5>
                                  <div>
                                    <i class="fa fa-edit" aria-hidden="true"></i>
                                    <i class="fa fa-info-circle" aria-hidden="true"
                                       data-container="body"
                                       data-toggle="popover" data-placement="top"
                                       data-trigger="hover"
                                       data-content="<?php echo $upaya['tanggal'] ?>">
                                    </i>
                                  </div>
                                </div>
                              </div>
                              <div class="card-body px-4">
                                <div class="graph-section">
                                  <p class="card-text">
                                    <?php echo htmlspecialchars_decode($upaya['ket']) ?>
                                  </p>
                                </div>

                                <form id="form-upaya" class="free-text-form" method="POST" action="form/insert_form_upaya.php"></form>
                                <div class="form-section py-0">
                                  <div class="form-group row">
                                    <label for="input-upaya" class="col-sm-12 col-form-label">Keterangan</label>
                                    <div class="col-sm-12">
                                      <textarea id="input-upaya" class="form-control input-editor" name="ket" form="form-upaya" required
                                                placeholder="Contoh: 10"><?php echo $upaya['ket'] ?></textarea>
                                    </div>
                                  </div>
                                  <div class="form-group row">
                                    <div class="input-group datepicker-input-group" id="fg-tanggal-upaya" data-target-input="nearest">
                                      <label for="input-tanggal" class="col-sm-4 col-form-label">Tanggal</label>
                                      <div class="col-sm-8">
                                        <input name="tanggal" id="input-tanggal" type="text" class="form-control datetimepicker-input" required
                                               data-target="#fg-tanggal-upaya" data-toggle="datetimepicker"
                                               form="form-upaya" placeholder="Contoh: 7 Mar 2021"/>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="form-group row d-flex justify-content-end">
                                    <input name="id-bencana" form="form-upaya" value="<?php echo $bencana_id ?>" hidden readonly>
                                    <button type="button" class="btn btn-default btn-cancel-form"><i class="fas fa-times"></i>
                                      Cancel
                                    </button>&nbsp;
                                    <button type="submit" class="btn btn-info" form="form-upaya"><i class="fa fa-save"></i>
                                      Simpan
                                    </button>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <!-- /.card -->
                          </div>
                          <!-- /.col-md-6 -->

                          <div class="col-lg-6 col-md-6 mb-3">
                            <div class="card card-warning h-100 info-card">
                              <div class="card-header">
                                <div class="d-flex justify-content-between">
                                  <h5 class="card-title m-0"><strong>Kebutuhan Mendesak di lokasi</strong></h5>
                                  <div>
                                    <i class="fa fa-edit" aria-hidden="true"></i>
                                    <i class="fa fa-info-circle" aria-hidden="true"
                                       data-container="body"
                                       data-toggle="popover" data-placement="top"
                                       data-trigger="hover"
                                       data-content="<?php echo $kebutuhan['tanggal'] ?>">
                                    </i>
                                  </div>
                                </div>
                              </div>
                              <div class="card-body px-4">
                                <div class="graph-section">
                                  <p class="card-text">
                                    <?php echo htmlspecialchars_decode($kebutuhan['ket']) ?>
                                  </p>
                                </div>

                                <form id="form-kebutuhan" class="free-text-form" method="POST" action="form/insert_form_kebutuhan.php"></form>
                                <div class="form-section py-0">
                                  <div class="form-group row">
                                    <label for="input-kebutuhan" class="col-sm-12 col-form-label">Keterangan</label>
                                    <div class="col-sm-12">
                                      <textarea id="input-kebutuhan" class="form-control input-editor" name="ket" form="form-kebutuhan" required
                                                placeholder="Contoh: 10"><?php echo $kebutuhan['ket'] ?></textarea>
                                    </div>
                                  </div>
                                  <div class="form-group row">
                                    <div class="input-group datepicker-input-group" id="fg-tanggal-kebutuhan" data-target-input="nearest">
                                      <label for="input-tanggal" class="col-sm-4 col-form-label">Tanggal</label>
                                      <div class="col-sm-8">
                                        <input name="tanggal" id="input-tanggal" type="text" class="form-control datetimepicker-input" required
                                               data-target="#fg-tanggal-kebutuhan" data-toggle="datetimepicker"
                                               form="form-kebutuhan" placeholder="Contoh: 7 Mar 2021"/>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="form-group row d-flex justify-content-end">
                                    <input name="id-bencana" form="form-kebutuhan" value="<?php echo $bencana_id ?>" hidden readonly>
                                    <button type="button" class="btn btn-default btn-cancel-form"><i class="fas fa-times"></i>
                                      Cancel
                                    </button>&nbsp;
                                    <button type="submit" class="btn btn-info" form="form-kebutuhan"><i class="fa fa-save"></i>
                                      Simpan
                                    </button>
                                  </div>
                                </div>
                              </div>

                            </div>
                            <!-- /.card -->
                          </div>
                          <!-- /.col-md-6 -->

                        </div>

                        <div class="row">

                          <div class="col-lg-6 col-md-6 mb-3">
                            <div class="card card-secondary h-100 info-card">
                              <div class="card-header">
                                <div class="d-flex justify-content-between">
                                  <h5 class="card-title m-0"><strong>Sumber Daya Tersedia di Lokasi</strong></h5>
                                  <div>
                                    <i class="fa fa-edit" aria-hidden="true"></i>
                                    <i class="fa fa-info-circle" aria-hidden="true"
                                       data-container="body"
                                       data-toggle="popover" data-placement="top"
                                       data-trigger="hover"
                                       data-content="<?php echo $daya['tanggal'] ?>">
                                    </i>
                                  </div>
                                </div>
                              </div>
                              <div class="card-body px-4">
                                <div class="graph-section">
                                  <p class="card-text">
                                    <?php echo htmlspecialchars_decode($daya['ket']) ?>
                                  </p>
                                </div>

                                <form id="form-daya" class="free-text-form" method="POST" action="form/insert_form_daya.php"></form>
                                <div class="form-section py-0">
                                  <div class="form-group row">
                                    <label for="input-daya" class="col-sm-12 col-form-label">Keterangan</label>
                                    <div class="col-sm-12">
                                      <textarea id="input-daya" class="form-control input-editor" name="ket" form="form-daya" required
                                                placeholder="Contoh: 10"><?php echo $daya['ket'] ?></textarea>
                                    </div>
                                  </div>
                                  <div class="form-group row">
                                    <div class="input-group datepicker-input-group" id="fg-tanggal-daya" data-target-input="nearest">
                                      <label for="input-tanggal" class="col-sm-4 col-form-label">Tanggal</label>
                                      <div class="col-sm-8">
                                        <input name="tanggal" id="input-tanggal" type="text" class="form-control datetimepicker-input" required
                                               data-target="#fg-tanggal-daya" data-toggle="datetimepicker"
                                               form="form-daya" placeholder="Contoh: 7 Mar 2021"/>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="form-group row d-flex justify-content-end">
                                    <input name="id-bencana" form="form-daya" value="<?php echo $bencana_id ?>" hidden readonly>
                                    <button type="button" class="btn btn-default btn-cancel-form"><i class="fas fa-times"></i>
                                      Cancel
                                    </button>&nbsp;
                                    <button type="submit" class="btn btn-info" form="form-daya"><i class="fa fa-save"></i>
                                      Simpan
                                    </button>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <!-- /.card -->
                          </div>
                          <!-- /.col-md-6 -->

                          <div class="col-lg-6 col-md-6 mb-3">
                            <div class="card card-info h-100 info-card">
                              <div class="card-header">
                                <div class="d-flex justify-content-between">
                                  <h5 class="card-title m-0"><strong>Kendala di
                                      Lokasi</strong></h5>
                                  <div>
                                    <i class="fa fa-edit" aria-hidden="true"></i>
                                    <i class="fa fa-info-circle" aria-hidden="true"
                                       data-container="body"
                                       data-toggle="popover" data-placement="top"
                                       data-trigger="hover"
                                       data-content="<?php echo $kendala['tanggal'] ?>">
                                    </i>
                                  </div>
                                </div>
                              </div>
                              <div class="card-body px-4">
                                <div class="graph-section">
                                  <p class="card-text">
                                    <?php echo htmlspecialchars_decode($kendala['ket']) ?>
                                  </p>
                                </div>

                                <form id="form-kendala" class="free-text-form" method="POST" action="form/insert_form_kendala.php"></form>
                                <div class="form-section py-0">
                                  <div class="form-group row">
                                    <label for="input-kendala" class="col-sm-12 col-form-label">Keterangan</label>
                                    <div class="col-sm-12">
                                      <textarea id="input-kendala" class="form-control input-editor" name="ket" form="form-kendala" required
                                                placeholder="Contoh: 10"><?php echo $kendala['ket'] ?></textarea>
                                    </div>
                                  </div>
                                  <div class="form-group row">
                                    <div class="input-group datepicker-input-group" id="fg-tanggal-kendala" data-target-input="nearest">
                                      <label for="input-tanggal" class="col-sm-4 col-form-label">Tanggal</label>
                                      <div class="col-sm-8">
                                        <input name="tanggal" id="input-tanggal" type="text" class="form-control datetimepicker-input" required
                                               data-target="#fg-tanggal-kendala" data-toggle="datetimepicker"
                                               form="form-kendala" placeholder="Contoh: 7 Mar 2021"/>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="form-group row d-flex justify-content-end">
                                    <input name="id-bencana" form="form-kendala" value="<?php echo $bencana_id ?>" hidden readonly>
                                    <button type="button" class="btn btn-default btn-cancel-form"><i class="fas fa-times"></i>
                                      Cancel
                                    </button>&nbsp;
                                    <button type="submit" class="btn btn-info" form="form-kendala"><i class="fa fa-save"></i>
                                      Simpan
                                    </button>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <!-- /.card -->
                          </div>
                          <!-- /.col-md-6 -->

                        </div>

                      </div>

                    </div>
                    <!-- /graphs -->

                  </div>

                </div>
                <!-- /.tab-pane -->

                <div class="tab-pane fade" id="pendataan-mendalam" role="tabpanel"
                     aria-labelledby="pendataan-mendalam-tab">

                  <div class="col-lg-12">
                    <div class="card mb-0 bg-transparent shadow-none">

                      <div class="card-header pr-0 pl-0 border-0">
                        <h2 class="m-0" style="width: 100%;text-align: center;">
                          <strong> Grafik Dampak Bencana</strong>
                        </h2>
                      </div>

                      <div class="card-body pr-0 pl-0">

                        <div class="row">

                          <div class="col-lg-6 col-md-6 mb-3">
                            <div class="card card-secondary h-100">
                              <div class="card-header border-0">

                                <div class="d-flex justify-content-between align-items-center">
                                  <h3 class="card-title">
                                    <strong>Kerusakan Rumah Terdampak</strong><br>
                                    <small>per tanggal 31 Desember 2020, 20.41
                                      PM</small>
                                  </h3>
                                  <div>
                                    <i class="fa fa-edit" aria-hidden="true"
                                       style="cursor: pointer;"
                                       data-container="body" data-toggle="modal"
                                       data-target="#modal-form-status-warga-terdampak"
                                       data-trigger="click">
                                    </i>
                                    <i class="fa fa-info-circle" aria-hidden="true"
                                       data-container="body"
                                       data-toggle="popover" data-placement="top"
                                       data-trigger="hover"
                                       data-content="Sumber data: Survey PU, 15 Jan 2021">
                                    </i>
                                  </div>
                                </div>
                              </div>

                              <div class="card-body">
                                <div class="chart">
                                  <canvas id="barChart-rumah"
                                          style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                                </div>
                              </div>

                            </div>
                            <!-- /.card -->
                          </div>
                          <!-- /.col-md-6 -->

                          <div class="col-lg-6 col-md-6 mb-3">
                            <div class="card card-warning h-100">
                              <div class="card-header border-0">

                                <div class="d-flex justify-content-between align-items-center">
                                  <h3 class="card-title">
                                    <strong>Status Kepemilikan Rumah
                                      Terdampak</strong><br>
                                    <small>per tanggal 31 Desember 2020, 20.41
                                      PM</small>
                                  </h3>
                                  <div>
                                    <i class="fa fa-edit" aria-hidden="true"
                                       style="cursor: pointer;"
                                       data-container="body" data-toggle="modal"
                                       data-target="#modal-form-status-warga-terdampak"
                                       data-trigger="click">
                                    </i>
                                    <i class="fa fa-info-circle" aria-hidden="true"
                                       data-container="body"
                                       data-toggle="popover" data-placement="top"
                                       data-trigger="hover"
                                       data-content="Sumber data: Survey PU, 15 Jan 2021">
                                    </i>
                                  </div>
                                </div>
                              </div>

                              <div class="card-body">
                                <div class="chart">
                                  <canvas id="barChart-status-rumah"
                                          style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                                </div>
                              </div>

                            </div>
                            <!-- /.card -->
                          </div>
                          <!-- /.col-md-6 -->

                        </div>

                        <div class="row">

                          <div class="col-lg-6 col-md-6 mb-3">
                            <div class="card card-danger h-100">
                              <div class="card-header border-0">

                                <div class="d-flex justify-content-between align-items-center">
                                  <h3 class="card-title">
                                    <strong>Status Lahan Terdampak</strong><br>
                                    <small>per tanggal 31 Desember 2020, 20.41
                                      PM</small>
                                  </h3>
                                  <div>
                                    <i class="fa fa-edit" aria-hidden="true"
                                       style="cursor: pointer;"
                                       data-container="body" data-toggle="modal"
                                       data-target="#modal-form-status-warga-terdampak"
                                       data-trigger="click">
                                    </i>
                                    <i class="fa fa-info-circle" aria-hidden="true"
                                       data-container="body"
                                       data-toggle="popover" data-placement="top"
                                       data-trigger="hover"
                                       data-content="Sumber data: Survey PU, 15 Jan 2021">
                                    </i>
                                  </div>
                                </div>
                              </div>

                              <div class="card-body">
                                <div class="chart">
                                  <canvas id="barChart-status-lahan"
                                          style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                                </div>
                              </div>

                            </div>
                            <!-- /.card -->
                          </div>
                          <!-- /.col-md-6 -->


                          <div class="col-lg-6 col-md-6 mb-3">
                            <div class="card card-info h-100">
                              <div class="card-header border-0">

                                <div class="d-flex justify-content-between align-items-center">
                                  <h3 class="card-title">
                                    <strong>Profil Penghuni Terdampak</strong><br>
                                    <small>per tanggal 31 Desember 2020, 20.41
                                      PM</small>
                                  </h3>
                                  <div>
                                    <i class="fa fa-edit" aria-hidden="true"
                                       style="cursor: pointer;"
                                       data-container="body" data-toggle="modal"
                                       data-target="#modal-form-status-warga-terdampak"
                                       data-trigger="click">
                                    </i>
                                    <i class="fa fa-info-circle" aria-hidden="true"
                                       data-container="body"
                                       data-toggle="popover" data-placement="top"
                                       data-trigger="hover"
                                       data-content="Sumber data: Survey PU, 15 Jan 2021">
                                    </i>
                                  </div>
                                </div>
                              </div>

                              <div class="card-body">
                                <div class="card-body">
                                  <canvas id="pieChart-gender"
                                          style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                                </div>
                              </div>

                            </div>
                            <!-- /.card -->
                          </div>
                          <!-- /.col-md-6 -->

                        </div>

                      </div>

                    </div>

                    <div class="card mb-0 bg-transparent shadow-none">

                      <div class="card-header pr-0 pl-0 border-0">
                        <h2 class="m-0" style="width: 100%;text-align: center;">
                          <strong> Peta Warga Terdampak</strong>
                        </h2>
                      </div>

                      <div class="card-body pr-0 pl-0">
                        <div class="row">

                          <div class="col-lg-12 col-sm-12 mb-3" style="min-height: 484px;">
                            <div id="map-card" class="card h-100 mb-0 px-0 container-fluid">
                              <div class="card-body p-0 mapbox">
                                <div class="row-fluid" id="second-map"
                                     style="height: 100%;"></div>
                                <div class="row-fluid overlay m-2">
                                  <input id="autoComplete" tabindex="1">
                                </div>
                              </div>
                            </div>
                            <!-- /.card -->
                          </div>
                          <!-- /.col-lg-8 col-sm-12 -->

                        </div>
                      </div>

                    </div>
                  </div>

                </div>
                <!-- /.tab-pane -->

              </div>
              <!-- /.card -->
            </div>
          </div>
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
      <!-- </div> -->
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <footer class="main-footer text-center bg-dark m-0">
      <!-- Default to the left -->
      Hak Cipta &copy; 2021 Kementerian Pekerjaan Umum dan Perumahan Rakyat Republik Indonesia. All rights
      reserved.
    </footer>
  </div>
  <!-- ./wrapper -->

  <div class="modal fade" id="modal-form-status-warga-terdampak">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Entri Data Status Warga Terdampak</h4>
        </div>
        <form id="form-status-warga-terdampak"></form>
        <div class="modal-body">
          <div class="form-group row">
            <label for="inputMeninggal" class="col-sm-3 col-form-label">Meniggal</label>
            <div class="col-sm-auto">
              <input type="number" class="form-control" id="inputMeninggal"
                     placeholder="Masukkan jumlah korban. e.g. 10" form="status-warga-terdampak">
            </div>
          </div>
          <div class="form-group row">
            <label for="inputLuka" class="col-sm-3 col-form-label">Luka</label>
            <div class="col-sm-auto">
              <input type="number" class="form-control" id="inputLuka"
                     placeholder="Masukkan jumlah korban. e.g. 10"
                     form="status-warga-terdampak">
            </div>
          </div>
          <div class="form-group row">
            <label for="inputHilang" class="col-sm-3 col-form-label">Hilang</label>
            <div class="col-sm-auto">
              <input type="number" class="form-control" id="inputHilang"
                     placeholder="Masukkan jumlah korban. e.g. 10"
                     form="status-warga-terdampak">
            </div>
          </div>
          <div class="form-group row">
            <label for="inputMengungsi" class="col-sm-3 col-form-label">Mengungsi</label>
            <div class="col-sm-auto">
              <input type="number" class="form-control" id="inputMengungsi"
                     placeholder="Masukkan jumlah korban. e.g. 10" form="status-warga-terdampak">
            </div>
          </div>
          <div class="form-group row" id="fg-tanggal-status-warga" data-target-input="nearest">
            <label for="inputTanggalStatusWarga" class="col-sm-3 col-form-label">Tanggal</label>
            <div class="col-sm-auto">
              <input id="inputTanggalStatusWarga" type="text" class="form-control datetimepicker-input"
                     data-target="#fg-tanggal-status-warga" data-toggle="datetimepicker"
                     form="status-warga-terdampak"
                     placeholder="Masukkan tanggal input. e.g. 7 Mar 2021"/>
            </div>
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="submit" class="btn btn-default float-right" form="status-warga-terdampak"
                  data-dismiss="modal"
                  aria-label="Close">Cancel
          </button>
          <button type="submit" class="btn btn-info" form="status-warga-terdampak"><i class="fa fa-save"></i>
            Simpan
          </button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>


  <div class="modal fade" id="modal-login">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header p-1 border-0">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">

          <form class="form-horizontal" action="api/login.php" method="post">
            <div class="card-body">
              <div class="form-group row justify-content-center my-4">
                <div class="col-sm-10">
                  <h2 class="text-center m-0">
                    <img src="static/frontend/dist/img/220px-Logo_PU_(RGB).jpg" alt="AdminLTE Logo"
                         class="brand-image elevation-3" style="opacity: .8" height="33">
                    <span class="brand-text">SIRUMBA</span>
                  </h2>
                </div>
              </div>
              <div class="form-group row justify-content-center">
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="username" placeholder="username" required>
                </div>
              </div>
              <div class="form-group row justify-content-center">
                <div class="col-sm-10">
                  <input type="password" class="form-control" name="password" placeholder="password" required>
                </div>
              </div>
              <div class="form-group row justify-content-center my-4">
                <div class="col-auto">
                  <button type="submit" class="btn btn-secondary rounded">Login</button>
                </div>
              </div>
            </div>
            <!-- /.card-body -->
          </form>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->

  <div class="modal fade" id="modal-xl">
    <div class="modal-dialog modal-dialog-centered modal-xl">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Informasi Bangunan</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-6">
              <p>One fine body&hellip;</p>
            </div>
            <div class="col-6">
              <p>One fine body&hellip;</p>
            </div>
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary">Save changes</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->

</div>

<!-- Page specific script -->
<script>
    var select2_data = [];
    var defaultDate = "<?php echo $date ?>";
    var currentDate = "<?php echo $current_date ?>"
    $.ajax({
        type: 'GET',
        url: 'api/select2_data.php',
        async: false,
        data: {
            balai_id: <?php echo $balai_id ?>
        },
        success: function (response) {
            select2_data = response['selectors'];
        }
    })

    $(function () {
        $('.card-header .fa-edit, .btn-cancel-form').on('click', function () {
            $(this).parents('.info-card').find('.graph-section').toggle()
            $(this).parents('.info-card').find('.form-section').toggle()
        })

        $('.input-editor').summernote({
            toolbar: [
                ['style', ['bold', 'italic', 'underline']],
                ['para', ['ul', 'ol']],
            ],
            codeviewFilter: true,
            codeviewIframeFilter: true,
            height: 200,
        });

        $('.free-text-form').on('submit', function (e) {
            e.preventDefault();
            $.ajax({
                type: "POST",
                data : $(this).serialize(),
                url: $(this).attr('action'),
                success: function(data){
                    if(!alert(data['message'])) {window.location.reload();}
                },
                error: function (data) {
                    alert(data['message']);
                }
            });
        })
    })
</script>
<script type="text/javascript" src="static/frontend/dist/js/page_filter.js"></script>

<!-- chart js script -->
<script>
    var pieChartData = <?php echo json_encode($pie_chart_data); ?>;
    var barChartData = <?php echo json_encode($bar_chart_data); ?>;

    var barChartRumahData = <?php echo json_encode($bar_chart_data); ?>;
    var barChartStatusRumahData = <?php echo json_encode($chart_status_rumah_data); ?>;
    var barChartStatusLahanData = <?php echo json_encode($chart_status_lahan_data); ?>;
    var pieChartGenderData = <?php echo json_encode($chart_gender_data); ?>;
</script>
<script type="text/javascript" src="static/frontend/dist/js/charts.js"></script>

<!-- Main Leaflet and Splide -->
<script>
    var bencana_id = <?php echo $bencana_id ?>;
</script>
<script type="text/javascript" src="static/frontend/dist/js/leaflet_accident.js"></script>

<!-- second leaflet -->
<script type="text/javascript" src="static/frontend/dist/js/leaflet_victim.js"></script>
</body>

</html>