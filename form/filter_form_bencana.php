<?php
$balai_id = (int)$_POST['inputBalai'];
$date = date('Y-m-d', strtotime($_POST['inputTanggal']));

$is_date_invalid = false === $date;
$is_balai_id_invalid = !filter_var($balai_id, FILTER_VALIDATE_INT) === false;
if ($is_date_invalid or $is_balai_id_invalid) {
  $error_message = "Input tidak valid";
  header("Location: 400.php?message=" . $error_message);
}

header("Location: ../index.php?balai_id=" . $balai_id . "&date=" . $date);
