<?php
if ($_SERVER['REQUEST_METHOD'] != 'POST') {
  exit;
}
header("Content-Type: application/json");

include("../api/conn.php");

include ("general/free_text_form.php");

$result = save_to_form('daya', $_POST, $conn);
echo json_encode($result);