<?php
if ($_SERVER['REQUEST_METHOD'] != 'POST') {
  exit;
}
header("Content-Type: application/json");

include("../api/conn.php");

$inputs = array(
  'lat' => (double)$_POST['lat'],
  'lng' => (double)$_POST['lng'],
);

$tanggal = date('Y-m-d', strtotime($_POST['tanggal']));
$bencana_id = $_POST['id-bencana'];
$sql = "SELECT mslink, balai from bencana where mslink = " . $bencana_id;
$sql_result = $conn->query($sql);
$bencana = mysqli_fetch_assoc($sql_result);

$is_exists_input_invalid = false;
foreach ($inputs as $key => $value) {
  $is_exists_input_invalid |= !is_double($value);
}
$is_exists_input_invalid |= false === $tanggal || !isset($bencana);

if ($is_exists_input_invalid) {
  http_response_code(400);
  $response = array(
    'message' => 'input tidak valid',
    'debug_tgl' => false === $tanggal,
    'debug_bencana'=> !isset($bencana),
    'debug_inputs' => $inputs
  );
} else {
  $inputs['bencana_id'] = $bencana["mslink"];

  $table_name = "bencana_marker";
  $insert_sql = "INSERT INTO " . $table_name . " (" . implode(", ", array_keys($inputs)) . ") " .
    "VALUES (" . implode(", ", array_values($inputs)) . ")";
  $sql_result = $conn->query($insert_sql);
  $marker_id = $conn->insert_id;

  $inputs['balai'] = $bencana['balai'];
  $inputs['tanggal'] = "'{$tanggal}'";

  $sql = "UPDATE balai SET tanggal = IF (tanggal <= '{$inputs['tanggal']}', '{$inputs['tanggal']}', tanggal) WHERE mslink={$inputs['balai']}";
  $sql_result = $conn->query($sql);

  $sql = "UPDATE bencana SET tanggal = IF (tanggal <= '{$inputs['tanggal']}', '{$inputs['tanggal']}', tanggal) WHERE mslink={$inputs['bencana_id']}";
  $sql_result = $conn->query($sql);

  $inputs['tanggal'] = date('d M Y', strtotime($tanggal));
  $response = array(
    'message' => 'marker bencana berhasil ditambahkan',
    'inputs' => $inputs,
    'id' => $marker_id,
  );
}

echo json_encode($response);
