<?php

function save_to_form($table_name, $post, $conn)
{
  try {
    $upaya_form = new FreeTextForm($table_name, $post, $conn);
    $inputs = $upaya_form->save_to_db();
    $response = array(
      'message' => "berhasil menambahkan input {$table_name}",
      'inputs' => $inputs
    );
  } catch (InvalidArgumentException $e) {
    $response = array(
      'message' => $e->getMessage()
    );
  }
  return ($response);
}

class FreeTextForm
{
  public $data;
  public $table_name;
  public $conn;
  public $is_valid_inputs;
  public $bencana;

  function __construct($table_name, $data, $conn)
  {
    $this->table_name = $table_name;
    $this->data = $data;
    $this->conn = $conn;

    $bencana_id = $data['id-bencana'];
    $sql = "SELECT mslink, balai from bencana where mslink = " . $bencana_id;
    $sql_result = $conn->query($sql);
    $this->bencana = mysqli_fetch_assoc($sql_result);

    $this->is_valid_inputs = $this->_clean();
  }

  function _clean()
  {
    if (!isset($this->conn)) {
      throw new InvalidArgumentException(
        'invalid db connection'
      );
    }

    $data = $this->data;
    $inputs = array(
      'ket' => "'" . htmlspecialchars($data['ket']) . "'",
    );
    $tanggal = date('Y-m-d', strtotime($data['tanggal']));

    if (!isset($this->is_valid_inputs)) {
      $is_exists_input_invalid = false;
      foreach ($inputs as $key => $value) {
        $is_exists_input_invalid |= !is_string($value);
      }
      $is_exists_input_invalid |= false === $tanggal || !isset($this->bencana);
      $this->is_valid_inputs = !$is_exists_input_invalid;

      if ($is_exists_input_invalid) {
        throw new InvalidArgumentException(
          'invalid input'
        );
      }
    }
    return ($this->is_valid_inputs);
  }

  function initiate_inputs($data, $conn)
  {
    $inputs = array();
    $ket = "'" . htmlspecialchars($data['ket']) . "'";
    $tanggal = date('Y-m-d', strtotime($data['tanggal']));
    $tanggal = "'" . $tanggal . "'";

    $bencana_id = $data['id-bencana'];
    $sql = "SELECT mslink, balai from bencana where mslink = " . $bencana_id;
    $sql_result = $conn->query($sql);
    $bencana = mysqli_fetch_assoc($sql_result);

    $inputs['ket'] = $ket;
    $inputs['tanggal'] = $tanggal;
    $inputs['kode_bencana'] = $bencana["mslink"];
    $inputs['balai'] = $bencana['balai'];

    return ($inputs);
  }

  function save_to_db()
  {
    $inputs = $this->initiate_inputs($this->data, $this->conn);
    $table_name = $this->table_name;
    $conn = $this->conn;

    $sql = "INSERT INTO " . $table_name . " (" . implode(", ", array_keys($inputs)) . ") " .
      "VALUES (" . implode(", ", array_values($inputs)) . ")";
    $insert_result = $conn->query($sql);

    if (!$insert_result) {
      throw new InvalidArgumentException(
        'failed to insert'
      );
    }

    $sql = "UPDATE balai SET tanggal = IF (tanggal <= {$inputs['tanggal']}, {$inputs['tanggal']}, tanggal) WHERE mslink={$inputs['balai']}";
    $update_balai_result = $conn->query($sql);

    if (!$update_balai_result) {
      throw new InvalidArgumentException(
        'failed to update balai'
      );
    }

    $sql = "UPDATE bencana SET tanggal = IF (tanggal <= {$inputs['tanggal']}, {$inputs['tanggal']}, tanggal) WHERE mslink={$inputs['kode_bencana']}";
    $update_bencana_result = $conn->query($sql);

    if (!$update_bencana_result) {
      throw new InvalidArgumentException(
        'failed to update bencana'
      );
    }

    return ($inputs);
  }
}