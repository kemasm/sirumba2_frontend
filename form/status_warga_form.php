<?php
/*
 * only allow access via post request
 */
if ($_SERVER['REQUEST_METHOD'] != 'POST') {
  exit;
}

include("../api/conn.php");

header("Content-Type: application/json");

/*
 * retrieve and filter inputs
 */
$inputs = array(
  'meninggal' => (int)$_POST['meninggal'],
  'luka' => (int)$_POST['luka'],
  'hilang' => (int)$_POST['hilang'],
  'mengungsi' => (int)$_POST['mengungsi'],
);

$tanggal = date('Y-m-d', strtotime($_POST['tanggal']));
$bencana_id = $_POST['id-bencana'];
$sql = "SELECT mslink, balai from bencana where mslink = ".$bencana_id;
$sql_result = $conn->query($sql);
$bencana = mysqli_fetch_assoc($sql_result);

$is_exists_input_invalid = false;
foreach ($inputs as $key => $value) {
  $is_exists_input_invalid |= !is_int($value);
}
$is_exists_input_invalid |= false === $tanggal || !isset($bencana);

if ($is_exists_input_invalid) {
  http_response_code(400);
  $response = array(
    'message' => 'input tidak valid'
  );
} else {
  /*
   * insert inputs and updates correlated tables
   */
  $inputs['tanggal'] = "'".$tanggal."'";
  $inputs['kode_bencana'] = $bencana["mslink"];
  $inputs['balai'] = $bencana['balai'];

  $table_name = "korban";
  $sql = "INSERT INTO ".$table_name." (".implode(", ", array_keys($inputs)).") ".
  "VALUES (".implode(", ", array_values($inputs)).")";
  $sql_result = $conn->query($sql);

  $sql = "UPDATE balai SET tanggal = IF (tanggal <= '{$inputs['tanggal']}', '{$inputs['tanggal']}', tanggal) WHERE mslink={$inputs['balai']}";
  $sql_result = $conn->query($sql);

  $sql = "UPDATE bencana SET tanggal = IF (tanggal <= '{$inputs['tanggal']}', '{$inputs['tanggal']}', tanggal) WHERE mslink={$inputs['kode_bencana']}";
  $sql_result = $conn->query($sql);

  $inputs['tanggal'] = date('d M Y', strtotime($tanggal));
  $response = array(
    'message' => 'status warga terdampak berhasil ditambahkan',
    'inputs' => $inputs
  );
}

echo json_encode($response);
